//
//  main.cpp
//  projet_mediatheque
//
//  Created by CB mac  on 10/11/2015.
//  Copyright © 2015 CB mac . All rights reserved.
//

#include <string>
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <sqlite3.h>
#include <fstream>
#include "Cd.hpp"
#include "Dvd.hpp"
#include "Livre.hpp"
#include "Createur.hpp"
#include "Utilisateur.hpp"
#include "Transaction.hpp"
#include "generatorId.hpp"

using namespace std;

//----------------------


//---------------------

int main(int argc, const char * argv[]) {
    
   
    
    cout<<"                   ---Bienvenue dans le gestionnaire de votre mediatheque---              "<<endl ;
    
    sqlite3* baseDeDonnee;
    
        int requete;
        const  char *sql_CD;
        const  char* sql_LIVRE ;
        const  char* sql_DVD;
        const  char* sql_CREATEUR ;
        const  char* sql_USER;
        const  char* sql_TRANSACTIONS ;
 
    
    requete = sqlite3_open("MEDIA.db", &baseDeDonnee);
        
    if( requete ){
        fprintf(stderr, "impossible d'ouvrir la base de donnee: %s\n", sqlite3_errmsg(baseDeDonnee));
        exit(0);
    }else{
        fprintf(stdout, "ouverture de la base de donnee reussi \n");
    }
        
  
//----------------------------------------CREATION DES TABLES---------------------------------------------
     sql_CD=" CREATE TABLE IF NOT EXISTS CD (" "ID TEXT PRIMARY KEY," "TITRE TEXT NOT NULL," " STUDIO TEXT NOT NULL," "DUREE INTEGER NOT NULL," "DATE TEXT NOT NULL," "`GENRE_1` TEXT NOT NULL," "DISPONIBILITE INTEGER NOT NULL," "AUTEUR TEXT DEFAULT INCONNU," "`GENRE_2` TEXT); " ;

     
     sql_LIVRE="CREATE TABLE IF NOT EXISTS LIVRE (" "ID TEXT PRIMARY KEY," "TITRE TEXT NOT NULL," "DUREE INTEGER NOT NULL," "EDITEUR TEXT NOT NULL," "DATE TEXT NOT NULL," "DISPONIBILITE INTEGER NOT NULL," "AUTEUR TEXT," "`GENRE_1` TEXT NOT NULL," "`GENRE_2` TEXT);";
     
  
    
    sql_DVD=" CREATE TABLE IF NOT EXISTS DVD (" "ID TEXT PRIMARY KEY," "TITRE TEXT NOT NULL," "STUDIO TEXT NOT NULL," "DUREE INTEGER NOT NULL," "DATE TEXT NOT NULL," "GENRE_1 TEXT NOT NULL," "DISPONIBILITE INTEGER NOT NULL," "AUTEUR TEXT ," "GENRE_2 TEXT DEFAULT AUCUN," "ACTEUR_1 TEXT NOT NULL," "ACTEUR_2 TEXT DEFAULT AUCUN," "ACTEUR_3 TEXT DEFAULT AUCUN);";
    
    
    sql_TRANSACTIONS="CREATE TABLE IF NOT EXISTS TRANSACTIONS (" "ID TEXT PRIMARY KEY," "ETAT INTEGER ," "DATE_PRET TEXT DEFAULT AUCUN," "DATE_RENDU TEXT DEFAULT AUCUN," "ARTICLE TEXT DEFAULT AUCUN," "UTILISATEUR TEXT DEFAULT AUCUN);" ;

    
    sql_CREATEUR="CREATE TABLE IF NOT EXISTS CREATEUR (" "ID TEXT PRIMARY KEY," "NOM TEXT NOT NULL," "PRENOM TEXT," "NATIONALITE TEXT , " "DATE TEXT," "STATUT_1 TEXT NOT NULL," "STATUT_2 TEXT DEFAULT AUCUN);";
    
    
    sql_USER="CREATE TABLE IF NOT EXISTS UTILISATEUR (" "ID TEXT PRIMARY KEY," "NOM TEXT ," "PRENOM TEXT ," "DATE_NAISSANCE TEXT , " "NUMERO_TELEPHONE TEXT," "ADRESSE TEXT ," "ARTICLE_EMPRUNTE_1 TEXT DEFAULT AUCUN," "ARTICLE_EMPRUNTE_2 TEXT DEFAULT AUCUN, " "ARTICLE_EMPRUNTE_3 TEXT DEFAULT AUCUN);" ;
  //-----------------------------------------------------------------------------------------------------
    
    
 
//---------------------------------------
    OperationBDD(sql_CD);
    OperationBDD(sql_DVD);
    OperationBDD(sql_LIVRE);
    
    OperationBDD(sql_USER);
    OperationBDD(sql_CREATEUR);
    
    OperationBDD(sql_TRANSACTIONS);
//----------------------------------------
    
    
//-------------------------------------------MENU MEDIATHEQUE--------------------------------------------
    int choix;
    string continuer ="OUI";
    
    while (continuer=="OUI")
    {
    cout << "   Vous vous interessez a ..." << endl;
    cout << "1. Un CD \n2. Un DVD\n3. Un Livre\n4. Un Createur\n5. Un Utilisateur\n6. Une Transaction" << endl;
    
    cin >> choix ;
    switch(choix)
    {
        case 1:
            menuCd();
            break;
        case 2:
             menuDvd();
             break;
        case 3:
             menuLivre() ;
             break;
        case 4 :
             menuCreateur() ;
             break;
        case 5:
             menuUtilisateur() ;
             break;
        case 6:
             menuTransaction();
             break;
        default:
             cout << "Commande entree incorrecte" << endl;
             break;
            
    }
        cout << "Voulez vous effectuer une autre action dans la base de donnee ? oui/non" << endl;
        cin >> continuer;
        majuscule(continuer);
    }
 //----------------------------------------------------------------------------------------------------------
    
    //-------------------LIBERATION TOTALE DE LA MEMOIRE UTILISE DANS LES COLLECTIONS---------------------
    Cd::deleteAllCd() ;
    Dvd::deleteAllDvd();
    Livre::deleteAllBook();
    Transaction::deleteAllTransaction();
    Utilisateur::deleteAllUser();
    Createur::deleteAllCreateur();
    
    //---------------------------------------------------------------------------------------------------
    
    return 0;
}





