//
//  generatorId.cpp
//  projet_mediatheque
//
//  Created by CB mac  on 08/12/2015.
//  Copyright © 2015 CB mac . All rights reserved.
//

#include "generatorId.hpp"
using namespace std;


string generer(const char& caractere ,int valeur){
    
    
    string str ;
    
    str= caractere+ int2String(valeur);
    return str ;
}


string int2String(int t){
    ostringstream os ;
    
    os<<t;
    return os.str() ;
}


//-----------------

int recupererId(){
    int code=0 ;
    fstream fichierId("Identifiant.txt");//ouvert avec 0 la premiere fois
    fichierId.seekg(ios::beg);
    fichierId>>code ;
    
    fichierId.seekg(ios::beg);// remettre en debut de fichier
    fichierId<<code+1 ;
    return code ;
}
//--------------

static int callback(void *data, int argc, char **argv, char **azColName){
    int i;
    fprintf(stderr, "%s", (const char*)data);
    for(i=0; i<argc; i++){
        printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
    }
    printf("\n");
    return 0;
}


void OperationBDD(const char* requete){
    
    sqlite3 *db;
    char *zErrMsg = 0;
    int rc;
  
    const char* data ="";
    
    
    rc = sqlite3_open("MEDIA.db", &db);

    rc = sqlite3_exec(db, requete, callback, (void*)data, &zErrMsg);
    if( rc != SQLITE_OK ){
        fprintf(stderr, "SQL error: %s\n", zErrMsg);
        sqlite3_free(zErrMsg);
    }else{
        //fprintf(stdout, "Operation reussie avec succes \n");
    }
    sqlite3_close(db);
    
    
}


//------------------


void majuscule(string& chaine){
    for (int i=0; i<chaine.length(); i++) {
        chaine[i]=toupper(chaine[i]);
    }
    
}



//-------------------------------------------











