//
//  Createur.hpp
//  projet_mediatheque
//
//  Created by CB mac  on 10/11/2015.
//  Copyright © 2015 CB mac . All rights reserved.
//

#ifndef Createur_hpp
#define Createur_hpp

#include <stdio.h>
#include <map>
#include "Personne.hpp"

#include <vector>

class Createur : public Personne {
private:
    std::string _nationalite ;
    std::vector <std::string> _statut ;
protected:
    static std::map<std::string,Createur*> _poolCreateur ;
    static unsigned _countCreateur;
    
    
public:
    Createur(const std::string& nom="",const std::string& prenom="",const std::string& nat="",const char& initiale='C');
    void setNationalite(const std::string& nationalite);
    std::string getNationalite()const ;
    void ajouterStatut(const std::string& statut);
    void afficherStatut()const;
    std::string getStatut(const int position) const;
    void setStatut(std::string& ,std::string&);
    virtual void afficher() const;
    
     ~Createur() ;
    static unsigned int getCountCreateur();
    
    
    static void deleteAllCreateur(); // libère tous les createur
    static Createur* getAdresseCreateur(const std::string& );
    static void addCreateur(Createur* createur); // enregistre createur dans le pool
    static void deleteCreateur(Createur* createur);
};

void menuCreateur() ;
Createur* ajouterCreateur() ;

void ChoixmodifCreateur(int choix, Createur* createur_a_modifier);
void ChoixmodifCreateurBDD(int choix,const std::string& idCREATEURamodifier );

#endif /* Createur_hpp */
