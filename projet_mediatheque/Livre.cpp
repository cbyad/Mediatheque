//
//  Livre.cpp
//  projet_mediatheque
//
//  Created by CB mac  on 16/11/2015.
//  Copyright © 2015 CB mac . All rights reserved.
//

#include "Livre.hpp"
using namespace std;



std::map<std::string,Livre*>Livre:: _poolBook ;
unsigned int Livre::_countBook=0 ;



Livre::Livre(const string& titre,const string& editeur,const int nombrepages,const Disponibilite& dispo,const char& initiale):Article(titre,nombrepages,dispo,initiale)
{
    _editeur=editeur ;
    _countBook++;

}



void Livre::afficher()const{// a completer
    
    cout<<"titre : "<<getTitre()<<endl;
    cout<<" editeur: "<<getEditeur()<<endl;
    cout<<getDate()<<endl ;


}




void Livre::setEditeur(const string& editeur){ _editeur=editeur ;}

string Livre::getEditeur() const {return  _editeur ;}




Livre::~Livre()
{
    deleteBook(this);
    _countBook--;
}





void Livre::deleteAllBook(){

    _poolBook.clear();

}



Livre* Livre::getBookAdresse(const std::string& id){

    std::map<std::string,Livre*>::iterator it= _poolBook.find(id) ;
    
    if (it==_poolBook.end()) {
        return NULL;
    }
    return it->second;

}


void Livre::addBoook(Livre* livre){

    if (livre!=NULL) {
        _poolBook[livre->getId()]=livre;
    }

}





void Livre::deleteBook(Livre *livre){

    if (livre!=NULL) {
        _poolBook.erase(livre->getId());
    }

}



std::string Livre::getStudio()const {return _editeur ;}



//------------------------------------


void menuLivre () {
    
    string continuer="OUI";
    
    int choix ;
    while (continuer=="OUI")
    {
        cin.clear();
        cin.ignore();
        cout << " 1. Ajouter un LIVRE \n 2. Supprimer un LIVRE \n 3. Modifier un LIVRE \n 4. Obtenir une information  \n 5. Supprimer tous les LIVREs '/!\' " << endl;
        cin >> choix;
        
        switch(choix)
        {
            case 1 :{
                cin.clear();
                cin.ignore();
                Livre* livre ;
                livre=ajouterLivre() ;
                string sql ;
                
                if(livre!=NULL){
                    
                    if(livre->getAuteur()!=NULL){
                 sql="INSERT INTO LIVRE (ID,TITRE,DUREE,EDITEUR,DATE,DISPONIBILITE,AUTEUR,GENRE_1,GENRE_2) " \
                "VALUES ('"+livre->getId()+"','"+livre->getTitre()+"','"+int2String(livre->getDuree())+"','"+livre->getEditeur()+"','"+livre->getDate()+"',"\
                        "'"+int2String(livre->getDisponibilite())+"','"+livre->getAuteur()->getNom()+"','"+livre->getGenre(0)+"' ,'"+livre->getGenre(1)+"'); ";
               
                
                    }
                    else{
                        sql="INSERT INTO LIVRE (ID,TITRE,DUREE,EDITEUR,DATE,DISPONIBILITE,AUTEUR,GENRE_1,GENRE_2) " \
                        "VALUES ('"+livre->getId()+"','"+livre->getTitre()+"','"+int2String(livre->getDuree())+"','"+livre->getEditeur()+"','"+livre->getDate()+"',"\
                        "'"+int2String(livre->getDisponibilite())+"','INCONNU','"+livre->getGenre(0)+"' ,'"+livre->getGenre(1)+"'); ";
                    
                    
                    }
                
                const char* conversionSql=sql.c_str() ;
                
            
                    OperationBDD(conversionSql); //ajout de l'info dans mediatheque.db
         
                }
                else cout<<"ajout de l'information impossible dans la base de donnee"<<endl ;
                break;
                
                
            }
                
            case 2 : {
                cin.clear();
                cin.ignore();
                string idLivreaSupprimer ;
                OperationBDD("SELECT ID,TITRE,AUTEUR from LIVRE");
                cout<<"saisir ID du LIVRE a supprimer:  \t" ;
                getline(cin,idLivreaSupprimer);
                Livre* livre =Livre::getBookAdresse(idLivreaSupprimer);
                
                string sql= "DELETE from LIVRE where ID='"+idLivreaSupprimer+"'" ;
                
                const char* conversionSql=sql.c_str() ;
                
                if (livre==NULL) {
                    
                    OperationBDD(conversionSql);     //supprimer bdd
                }
                else {
                    Livre::deleteBook(livre) ;//supprimer du pool
                    OperationBDD(conversionSql);    //supprimer bdd
                    
                }
            
                break ;
            }
            case 3:{
                
                cin.clear();
                cin.ignore();
                string idLIVREamodifier ;
                OperationBDD("SELECT ID,TITRE,AUTEUR from DVD");
                cout<<"Saisissez l'id du LIVRE a modifier \t";
                getline(cin,idLIVREamodifier);
                Livre* livre = Livre::getBookAdresse(idLIVREamodifier);
                if (livre == NULL) {
                    cout << "Vous souhaitez modifier ..." << endl;
                    cout << "1. Le titre\n2. La date de sortie\n3. L'auteur\n4. La duree\n5. L'editeur\n6. Le genre" << endl;
                    cin >> choix;
                    ChoixmodifLivreBDD(choix,idLIVREamodifier);
                    
                    
                }
                else{
                    cout << "Vous souhaitez modifier ..." << endl;
                    cout << "1. Le titre\n2. La date de sortie\n3. L'auteur\n4. La duree\n5. L'editeur\n6. Le genre" << endl;
                    cin >> choix;
                    ChoixmodifLivre(choix,livre);
                }

                break ;
            }
            case 4:{
                cin.clear();
                cin.ignore();
                string idLIVREaAfficher ;
                OperationBDD("SELECT ID,TITRE,AUTEUR from LIVRE");
                cout<<"saisir ID du LIVRE a afficher:  \t" ;
                getline(cin,idLIVREaAfficher);
                
                string sql= "SELECT * from LIVRE where ID='"+idLIVREaAfficher+"'" ;//
                
                const char* conversionSql=sql.c_str() ;
                OperationBDD(conversionSql);
                break ;
            }
            case 5:{
                Livre::deleteAllBook() ;
                OperationBDD("DELETE  from LIVRE ");
                break ;
            }
                
            default:
                
                break ;
            
        }
        cout << "Souhaitez vous avoir une autre action sur un LIVRE ? oui/non \t " ;
        cin >> continuer;
        majuscule(continuer);
    }
    
}



Livre* ajouterLivre() {
    cin.clear();
    //-------------
    int jour , mois ,annee ;
    string genre; string reponse ;// pour ajouter un autre genre de LIVRE
    string idAuteur ; string titre ; string editeur ;
    
    unsigned int duree ;
    
   
    //------------
    
    cout<<"saisir le titre"<<endl;
    getline(cin,titre);
    cout<<"saisir la duree"<<endl;
    cin>>duree;
    cin.ignore() ;//vider le buffer
    cout<<"saisir le nom du editeur"<<endl;
    getline(cin,editeur);

    Livre* livre=new Livre(titre,editeur,duree);
   
    cout<<"saisir la date de Publication du LIVRE"<<endl ;
    cout<<"le jour : \t";cin>>jour ;
    cout<<"le mois : \t";cin>>mois ;
    cout<<"l'annee : \t";cin>>annee ;
    
    livre->setDate(jour, mois, annee);
    //--------
    cin.clear();
    cin.ignore();
    //--------
    cout<<"saisir l'identifiant de l'auteur du LIVRE"<<endl;
    getline(cin,idAuteur);
    livre->associer(idAuteur); // penser au cas ou le pool Createur serait vide! basculer a la bdd
    
    cout<<"ajoutez un genre particulier \t" ;
    getline(cin,genre);
    livre->ajoutGenre(genre);
    
    cout<<"souhaitez vous ajoutez un autre genre particulier ? oui/non \t";
    
    getline(cin,reponse);
    majuscule(reponse);
    
    if (reponse=="OUI") {
        cout<<"saisir le genre \t ";
        getline(cin,genre);
        livre->ajoutGenre(genre);
    }
    
    return livre ;
    
}


void ChoixmodifLivre(int choix, Livre* livre_a_modifier) //Implémenter les modif dans la base de donnée.
{
    switch(choix){
            
        case 1 :
        {
            cin.clear();
            cin.ignore();
            string titre;
            cout << "Saisir le titre" << endl;
            getline(cin,titre);
            livre_a_modifier->setTitre(titre);
            
            string sql="UPDATE LIVRE SET TITRE='"+titre+"' WHERE ID='"+livre_a_modifier->getId()+"' ";
            const char* conversionSql=sql.c_str() ;
            OperationBDD(conversionSql);
            break;
        }
        case 2 :
        {
            cin.clear();
            cin.ignore();
            int jour,mois,annee;
            cout << "Saisir la nouvelle date de creation du cd :" << endl ;
            cout << "Le jour : \t " ;cin >> jour ;
            cout << "Le mois : \t"; cin >> mois ;
            cout << "L'annee : \t";cin >> annee ;
            livre_a_modifier->setDate(jour, mois, annee);
            string date=livre_a_modifier->getDate();
            
            string sql="UPDATE DVD SET DATE='"+date+"' WHERE ID='"+livre_a_modifier->getId()+"'";
            const char* conversionSql=sql.c_str() ;
            OperationBDD(conversionSql);
            
            break;
        }
        case 3 :
        {
            cin.clear();
            cin.ignore();
            string IDartiste;
            cout << "Entrez l'id de l'artiste" << endl;
            getline(cin,IDartiste);
            livre_a_modifier->associer(IDartiste);
            string nomArtiste=livre_a_modifier->getAuteur()->getNom() ;
            
            string sql="UPDATE DVD SET AUTEUR='"+nomArtiste+"' WHERE ID='"+livre_a_modifier->getId()+"' ";
            const char* conversionSql=sql.c_str() ;
            OperationBDD(conversionSql);
            
            break;
        }
        case 4 :
        {
            cin.clear();
            cin.ignore();
            int duree;
            cout << "Entrez la nouvelle durée" << endl;
            cin >> duree;
            livre_a_modifier->setDuree(duree);
            
            string sql="UPDATE DVD SET DUREE="+int2String(livre_a_modifier->getDuree())+" WHERE ID='"+livre_a_modifier->getId()+"' ";
            const char* conversionSql=sql.c_str() ;
            OperationBDD(conversionSql);
            break;
        }
        case 5 :
        {
            cin.clear();
            cin.ignore();
            string editeur;
            cout << "Entrez le nouveau editeur" << endl;
            getline(cin,editeur);
            livre_a_modifier->setEditeur(editeur);
            
            string sql="UPDATE DVD SET EDITEUR='"+editeur+"' WHERE ID='"+livre_a_modifier->getId()+"' ";
            const char* conversionSql=sql.c_str() ;
            OperationBDD(conversionSql);
            break;
        }
        case 6 :
        {
            cin.clear();
            cin.ignore();
            string genre1, genre2;
            cout << "Merci de redefinir le genre, avec un maximum de deux attributs" << endl;
            getline(cin,genre1);
            getline(cin,genre2);
            livre_a_modifier->setGenre(genre1, genre2);
            
            string sql="UPDATE DVD SET GENRE_1='"+genre1+"' WHERE ID='"+livre_a_modifier->getId()+"' ";
            const char* conversionSql=sql.c_str() ;
            OperationBDD(conversionSql);
            
            string sql2="UPDATE DVD SET GENRE_2='"+genre2+"' WHERE ID='"+livre_a_modifier->getId()+"' ";
            const char* conversionSql2=sql2.c_str() ;
            OperationBDD(conversionSql2);
            
            break;
        }
            
    }
}


void ChoixmodifLivreBDD(int choix,const string& idDLIVREamodifier ){
    
    
    switch(choix){
        case 1 :
        {
            cin.clear();
            cin.ignore();
            string titre;
            cout << "Saisir le titre " << endl;
            getline(cin,titre);
            
            string sql="UPDATE DVD SET TITRE='"+titre+"' WHERE ID='"+idDLIVREamodifier+"' ";
            const char* conversionSql=sql.c_str() ;
            OperationBDD(conversionSql);
            
            break;
        }
        case 2 :
        {
            cin.clear();
            cin.ignore();
            int jour,mois,annee;
            cout << "Saisir la nouvelle date de creation du livre :" << endl ;
            cout << "Le jour : \t " ;cin >> jour ;
            cout << "Le mois : \t"; cin >> mois ;
            cout << "L'annee : \t";cin >> annee ;
            
            string date=int2String(jour)+"/"+int2String(mois)+"/"+int2String(annee);
            
            string sql="UPDATE LIVRE SET DATE='"+date+"' WHERE ID='"+idDLIVREamodifier+"'";
            const char* conversionSql=sql.c_str() ;
            OperationBDD(conversionSql);
            
            break;
        }
        case 3 :
        {
            cin.clear();
            cin.ignore();
            string nomAuteur;
            cout << "Entrez l'id de l'artiste" << endl;
            getline(cin,nomAuteur);
            
            string sql="UPDATE LIVRE SET AUTEUR='"+nomAuteur+"' WHERE ID='"+idDLIVREamodifier+"' ";
            const char* conversionSql=sql.c_str() ;
            OperationBDD(conversionSql);
            
            break;
        }
        case 4 :
        {
            cin.clear();
            cin.ignore();
            int duree;
            cout << "Entrez la nouvelle durée" << endl;
            cin >> duree;
            
            string sql="UPDATE LIVRE SET DUREE="+int2String(duree)+" WHERE ID='"+idDLIVREamodifier+"' ";
            const char* conversionSql=sql.c_str() ;
            OperationBDD(conversionSql);
            
            
            break;
        }
        case 5 :
        {
            cin.clear();
            cin.ignore();
            string studio;
            cout << "Entrez le nouveau studio" << endl;
            getline(cin,studio);
            
            
            string sql="UPDATE LIVRE SET STUDIO='"+studio+"' WHERE ID='"+idDLIVREamodifier+"' ";
            const char* conversionSql=sql.c_str() ;
            OperationBDD(conversionSql);
            break;
        }
        case 6 :
        {
            cin.clear();
            cin.ignore();
            string genre1, genre2;
            cout << "Merci de redefinir le genre, avec un maximum de deux attributs" << endl;
            getline(cin,genre1);
            getline(cin,genre2);
            
            
            string sql="UPDATE LIVRE SET GENRE_1='"+genre1+"' WHERE ID='"+idDLIVREamodifier+"' ";
            const char* conversionSql=sql.c_str() ;
            OperationBDD(conversionSql);
            
            string sql2="UPDATE LIVRE SET GENRE_2='"+genre2+"' WHERE ID='"+idDLIVREamodifier+"' ";
            const char* conversionSql2=sql2.c_str() ;
            OperationBDD(conversionSql2);
            
            
            break;
        }
            
    }
    
    
}














