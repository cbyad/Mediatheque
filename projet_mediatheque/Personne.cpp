//
//  Personne.cpp
//  projet_mediatheque
//
//  Created by CB mac  on 10/11/2015.
//  Copyright © 2015 CB mac . All rights reserved.
//

#include "Personne.hpp"
using namespace std;

unsigned int Personne::_count=0 ;

unsigned int Personne::getPersonneCount(){return _count ;}

Personne::Personne(const string& nom,const string& prenom,const char& initiale):_nom(nom),_prenom(prenom)

{
    _id=generer(initiale, recupererId());
    _count++ ;
}


string Personne::getId() const { return _id ;  }

void Personne::setNom(const std::string& nom){_nom=nom ;}

string Personne::getNom() const {return _nom ;}

void Personne::setPrenom(const std::string& prenom){_prenom=prenom ;}

string Personne::getPrenom()const {return _prenom ;}

Personne::~Personne() {
    _count-- ;
}


void Personne::setDate(const int jour, const int mois , const int annee){
    _date.setJour(jour);
    _date.setMois(mois);
    _date.setAnnee(annee);
    
    
}

string Personne::getDate() const{
    string str ;
    int j=_date.getjour() , m=_date.getMois() ,a=_date.getAnnee() ;
    
  
    str=int2String(j)+"/"+int2String(m)+"/"+int2String(a);
    return str ;
    
}



