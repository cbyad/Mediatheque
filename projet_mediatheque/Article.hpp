//
//  Article.hpp
//  projet_mediatheque
//
//  Created by CB mac  on 10/11/2015.
//  Copyright © 2015 CB mac . All rights reserved.
//

#ifndef Article_hpp
#define Article_hpp

#include <stdio.h>
#include <vector>
#include <iostream>
#include "Date.hpp"
#include "Createur.hpp"
#include "generatorId.hpp"

enum Disponibilite {
 DISPONIBLE , EMPRUNTE
};

typedef enum Disponibilite Disponibilite ;

class Utilisateur;

class Article {
private:
    std::string _titre ;
    Date _date ;
    Createur* _auteur ;
    std::vector <std::string> _genres;
    int _duree;
    std::string _id ;
    
    //--------------
    Utilisateur* _utilisateur ;// necessaire finalement ? je ne pense pas
    Disponibilite _disponibilite ; // ok
    //--------------
    
protected:
    static unsigned int _count ;

public:
    static unsigned int getArticleCount() ;
    Article(const std::string& titre=" ",const  int duree=0 , const Disponibilite& dispo=DISPONIBLE,const char& initiale='A');
    
    void setTitre(const std::string& titre);
    void setDate(const int jour, const int mois , const int annee);
    void setDuree(const int duree);
    void associer(const std::string& idAuteur);
    Createur* getAuteur()const ; // add 17dec
    void pretArticle(const std::string& idUser);//////////////////
    void ajoutGenre(const std::string& genre);
    void afficherGenre() const ;
    void setGenre(std::string& genre1, std::string& genre2);
    std::string getGenre(const int position) const ;
    std::string getTitre()const ;
    std::string getDate() const ;
    int getDuree()const ;
    std::string getId() const ;
    Disponibilite getDisponibilite()const ;
    void setDisponibilite(const Disponibilite dispo);
    virtual void afficher() const = 0 ;
    virtual std::string getStudio() const =0;
    virtual  ~Article();


};




#endif /* Article_hpp */
