//
//  Dvd.cpp
//  projet_mediatheque
//
//  Created by CB mac  on 16/11/2015.
//  Copyright © 2015 CB mac . All rights reserved.
//

#include "Dvd.hpp"
using namespace std;

//--------

unsigned int Dvd::_countDvd=0 ;
map<string,Dvd*> Dvd::_poolDvd ;


//------

Dvd:: Dvd(const std::string& titre, const unsigned int duree,const std::string& studio,const Disponibilite& dispo,const char& initiale)
:Article(titre,duree,dispo,initiale),_Studio(studio)
{
    addDvd(this);
    _countDvd++;
}

void Dvd::setStudio(const std::string& studio){_Studio=studio;}

std::string Dvd::getStudio()const {return _Studio ;}



void Dvd::setCasting(const std::string& acteur){
    _castings.push_back(acteur);
    
}




void Dvd::printCasting()  const {
    if(_castings.size()!=0){
        cout<<"acteur(s) :"<<endl;
    for(int i=0;i<_castings.size();i++)
    cout<<i+1<<"-"<<_castings[i]<<endl ;
    
    }
    else
        cout<<"aucun acteur enregistre"<<endl ;
}

std::string Dvd::getCasting(const int position) const {
    if (_castings.size()!=0 && position<_castings.size()) {
        
        return _castings[position];
    }
    return "VIDE" ;

}



void Dvd::afficher() const
{
    cout << "Titre: " << getTitre() << endl;
    cout << "Duree: " << getDuree() << endl;
    cout << "Studio: " << getStudio() << endl;
    
    printCasting();
    cout<<getDate()<<endl ;
    cout << "Disponibilite: " << getDisponibilite() << endl;
}




Dvd::~Dvd() {
    deleteDvd(this);
    _countDvd--;
}


void Dvd::deleteAllDvd(){ _poolDvd.clear();}



Dvd* Dvd::getDvdAdresse(const std::string& id){
    map<string,Dvd*>::iterator it=_poolDvd.find(id);
    
    
    if (it==_poolDvd.end()) {
        return NULL;
    }

    return it->second ;

}




void Dvd::addDvd(Dvd* dvd){
    if(dvd!=NULL) _poolDvd[dvd->getId()]=dvd;
}





void Dvd::deleteDvd(Dvd* dvd){

   if(dvd!=NULL) _poolDvd.erase(dvd->getId());

}

//----------------------


void menuDvd () {
    string continuer="OUI";
    
    int choix ;
    while (continuer=="OUI")
    {
        cin.clear();
        cin.ignore();
        cout << " 1. Ajouter un DVD \n 2. Supprimer un DVD \n 3. Modifier un DVD \n 4. Obtenir une information  \n 5. Supprimer tous les DVDs '/!\' " << endl;
        cin >> choix;
        
        switch(choix)
        {
            case 1 :{
                cin.clear();
                cin.ignore();

                Dvd* dvd ;
                dvd=ajouterDvd() ;
         
                string sql ;
            
                
                if(dvd != NULL){
                    
                  
                    
                    if(dvd->getAuteur()!=NULL){
                        sql="INSERT INTO DVD (ID,TITRE,STUDIO,DUREE,DATE,GENRE_1,DISPONIBILITE,AUTEUR,GENRE_2,ACTEUR_1,ACTEUR_2,ACTEUR_3) " \
                        "VALUES ('"+dvd->getId()+"','"+dvd->getTitre()+"','"+dvd->getStudio()+"','"+int2String(dvd->getDuree())+"','"+dvd->getDate()+"',"\
                        "'"+dvd->getGenre(0)+"','"+int2String(dvd->getDisponibilite())+"','"+dvd->getAuteur()->getNom()+"' ,'"+dvd->getGenre(1)+"','"+dvd->getCasting(0)+"','"+dvd->getCasting(1)+"','"+dvd->getCasting(2)+"') ";
                        
                    }
                    else{
                        sql="INSERT INTO DVD (ID,TITRE,STUDIO,DUREE,DATE,GENRE_1,DISPONIBILITE,AUTEUR,GENRE_2,ACTEUR_1,ACTEUR_2,ACTEUR_3) " \
                        "VALUES ('"+dvd->getId()+"','"+dvd->getTitre()+"','"+dvd->getStudio()+"','"+int2String(dvd->getDuree())+"','"+dvd->getDate()+"',"\
                        "'"+dvd->getGenre(0)+"','"+int2String(dvd->getDisponibilite())+"','INCONNU','"+dvd->getGenre(1)+"','"+dvd->getCasting(0)+"','"+dvd->getCasting(1)+"','"+dvd->getCasting(2)+"') ";
                    }
                    
                    const char* conversionSql = sql.c_str() ;
                    
                    
                    OperationBDD(conversionSql); //ajout de l'info dans mediatheque.db
                }
                
                else cout << "Ajout de l'information impossible dans la base de donnee" << endl ;
                break;
            }
            case 2 : {
                cin.clear();
                cin.ignore();
                string idDVDaSupprimer ;
                OperationBDD("SELECT ID,TITRE,AUTEUR from DVD");
                cout<<"saisir ID du DVD a supprimer:  \t" ;
                getline(cin,idDVDaSupprimer);
                Dvd* dvd =Dvd::getDvdAdresse(idDVDaSupprimer);
                
                string sql= "DELETE from DVD where ID='"+idDVDaSupprimer+"'" ;
                
                const char* conversionSql=sql.c_str() ;
                
                if (dvd==NULL) {
                    
                    OperationBDD(conversionSql);     //supprimer bdd
                }
                else {
                    Dvd::deleteDvd(dvd) ;//supprimer du pool
                    OperationBDD(conversionSql);    //supprimer bdd
                    
                }
                
                break ;
            }
            case 3:{
               
                
                cin.clear();
                cin.ignore();
                string idDVDamodifier ;
                OperationBDD("SELECT ID,TITRE,AUTEUR from DVD");
                cout<<"Saisissez l'id du DVD a modifier \t";
                getline(cin,idDVDamodifier);
                Dvd* dvd = Dvd::getDvdAdresse(idDVDamodifier);
                if (dvd == NULL) {
                    cout << "Vous souhaitez modifier ..." << endl;
                    cout << "1. Le titre\n2. La date de sortie\n3. L'artiste\n4. La duree\n5. Le studio\n6. Le genre" << endl;
                    cin >> choix;
                    ChoixmodifDvdBDD(choix,idDVDamodifier);
                    
                    
                }
                else{
                    cout << "Vous souhaitez modifier ..." << endl;
                    cout << "1. Le titre\n2. La date de sortie\n3. L'artiste\n4. La duree\n5. Le studio\n6. Le genre" << endl;
                    cin >> choix;
                    ChoixmodifDvd(choix,dvd);
                }

                break ;
            }
            case 4:{
                string idDVDaAfficher ;
                OperationBDD("SELECT ID,TITRE,AUTEUR from DVD");
                cout<<"saisir ID du DVD a afficher:  \t" ;
                getline(cin,idDVDaAfficher);
                
                string sql= "SELECT * from DVD where ID='"+idDVDaAfficher+"';" ;//
                
                const char* conversionSql=sql.c_str() ;
                OperationBDD(conversionSql);
                break ;
            }
            case 5:{
                Dvd::deleteAllDvd() ;
                OperationBDD("DELETE  from DVD ;");
                break ;
            }
                
            default:
                break ;
        }
        cout << "Souhaitez vous avoir une autre action sur un DVD ? oui/non" << endl;
        cin >> continuer;
        majuscule(continuer);
    }
    
}


Dvd* ajouterDvd() {
    //-------------
    cin.clear();
    //cin.ignore();
    //--------------
    
    int jour , mois ,annee ;
    string casting ;
    string genre; string reponse ;// pour ajouter un autre genre de cd
    string idAuteur ; string titre ; string studio ;
    
    unsigned int duree ;
    
   
    cout<<"saisir le titre"<<endl;
    getline(cin,titre);
    cout<<"saisir la duree"<<endl;
    cin>>duree;
    cin.ignore() ;//vider le buffer
    cout<<"saisir le nom du studio"<<endl;
    getline(cin,studio);
    
    Dvd* dvd=new Dvd(titre,duree,studio);
 
    
    cout<<"saisir la date de creation du dvd"<<endl ;
    cout<<"le jour : \t";cin>>jour ;
    cout<<"le mois : \t";cin>>mois ;
    cout<<"l'annee : \t";cin>>annee ;
    
    dvd->setDate(jour, mois, annee);
    //--------
    cin.clear();
    cin.ignore();
    //--------
    cout<<"saisir l'identifiant de l'auteur du dvd"<<endl;
    getline(cin,idAuteur);
    dvd->associer(idAuteur); // penser au cas ou le pool Createur serait vide! basculer a la bdd
    
    cout<<"ajoutez un genre particulier \t" ;
    getline(cin,genre);
    dvd->ajoutGenre(genre);
    
    cout<<"souhaitez vous ajoutez un autre genre particulier ? oui/non \t";
    
    getline(cin,reponse);
    majuscule(reponse);
    
    if (reponse=="OUI") {
        cout<<"saisir le genre \t ";
        getline(cin,genre);
        dvd->ajoutGenre(genre);
    }
    
    //ajout liste acteur
    
    return dvd ;
    
}


void ChoixmodifDvd(int choix, Dvd* dvd_a_modifier) //Implémenter les modif dans la base de donnée.
{
    switch(choix){
            
        case 1 :
        {
            cin.clear();
            cin.ignore();
            string titre;
            cout << "Saisir le titre" << endl;
            getline(cin,titre);
            dvd_a_modifier->setTitre(titre);
            
            string sql="UPDATE DVD SET TITRE='"+titre+"' WHERE ID='"+dvd_a_modifier->getId()+"' ";
            const char* conversionSql=sql.c_str() ;
            OperationBDD(conversionSql);
            
            break;
        }
        case 2 :
        {
            cin.clear();
            cin.ignore();
            int jour,mois,annee;
            cout << "Saisir la nouvelle date de creation du dvd :" << endl ;
            cout << "Le jour : \t " ;cin >> jour ;
            cout << "Le mois : \t"; cin >> mois ;
            cout << "L'annee : \t";cin >> annee ;
            dvd_a_modifier->setDate(jour, mois, annee);
            string date=dvd_a_modifier->getDate();
            
            string sql="UPDATE DVD SET DATE='"+date+"' WHERE ID='"+dvd_a_modifier->getId()+"'";
            const char* conversionSql=sql.c_str() ;
            OperationBDD(conversionSql);
            
            break;
        }
        case 3 :
        {
            cin.clear();
            cin.ignore();
            string IDartiste;
            cout << "Entrez l'id de l'artiste" << endl;
            getline(cin,IDartiste);
            dvd_a_modifier->associer(IDartiste);
            string nomArtiste=dvd_a_modifier->getAuteur()->getNom() ;
            
            string sql="UPDATE DVD SET AUTEUR='"+nomArtiste+"' WHERE ID='"+dvd_a_modifier->getId()+"' ";
            const char* conversionSql=sql.c_str() ;
            OperationBDD(conversionSql);
            
            break;
        }
        case 4 :
        {
            cin.clear();
            cin.ignore();
            int duree;
            cout << "Entrez la nouvelle durée" << endl;
            cin >> duree;
            dvd_a_modifier->setDuree(duree);
            
            string sql="UPDATE DVD SET DUREE="+int2String(dvd_a_modifier->getDuree())+" WHERE ID='"+dvd_a_modifier->getId()+"' ";
            const char* conversionSql=sql.c_str() ;
            OperationBDD(conversionSql);
            
            
            break;
        }
        case 5 :
        {
            cin.clear();
            cin.ignore();
            string studio;
            cout << "Entrez le nouveau studio" << endl;
            getline(cin,studio);
            dvd_a_modifier->setStudio(studio);
            
            string sql="UPDATE DVD SET STUDIO='"+studio+"' WHERE ID='"+dvd_a_modifier->getId()+"' ";
            const char* conversionSql=sql.c_str() ;
            OperationBDD(conversionSql);
            break;
        }
        case 6 :
        {
            cin.clear();
            cin.ignore();
            string genre1, genre2;
            cout << "Merci de redefinir le genre, avec un maximum de deux attributs" << endl;
            getline(cin,genre1);
            getline(cin,genre2);
            dvd_a_modifier->setGenre(genre1, genre2);
            
            string sql="UPDATE DVD SET GENRE_1='"+genre1+"' WHERE ID='"+dvd_a_modifier->getId()+"' ";
            const char* conversionSql=sql.c_str() ;
            OperationBDD(conversionSql);
            
            string sql2="UPDATE DVD SET GENRE_2='"+genre2+"' WHERE ID='"+dvd_a_modifier->getId()+"' ";
            const char* conversionSql2=sql2.c_str() ;
            OperationBDD(conversionSql2);
            
            
            break;
        }
            
    }
}


void ChoixmodifDvdBDD(int choix,const string& idDVDamodifier ){
    
    
    switch(choix){
        case 1 :
        {
            cin.clear();
            cin.ignore();
            string titre;
            cout << "Saisir le titre " << endl;
            getline(cin,titre);
            
            string sql="UPDATE DVD SET TITRE='"+titre+"' WHERE ID='"+idDVDamodifier+"' ";
            const char* conversionSql=sql.c_str() ;
            OperationBDD(conversionSql);
            
            break;
        }
        case 2 :
        {
            cin.clear();
            cin.ignore();
            int jour,mois,annee;
            cout << "Saisir la nouvelle date de creation du dvd :" << endl ;
            cout << "Le jour : \t " ;cin >> jour ;
            cout << "Le mois : \t"; cin >> mois ;
            cout << "L'annee : \t";cin >> annee ;
            
            string date=int2String(jour)+"/"+int2String(mois)+"/"+int2String(annee);
            
            
            string sql="UPDATE DVD SET DATE='"+date+"' WHERE ID='"+idDVDamodifier+"'";
            const char* conversionSql=sql.c_str() ;
            OperationBDD(conversionSql);
            
            break;
        }
        case 3 :
        {
            cin.clear();
            cin.ignore();
            string nomArtiste;
            cout << "Entrez l'id de l'artiste" << endl;
            getline(cin,nomArtiste);
            
            
            string sql="UPDATE DVD SET AUTEUR='"+nomArtiste+"' WHERE ID='"+idDVDamodifier+"' ";
            const char* conversionSql=sql.c_str() ;
            OperationBDD(conversionSql);
            
            break;
        }
        case 4 :
        {
            cin.clear();
            cin.ignore();
            int duree;
            cout << "Entrez la nouvelle durée" << endl;
            cin >> duree;
            
            string sql="UPDATE DVD SET DUREE="+int2String(duree)+" WHERE ID='"+idDVDamodifier+"' ";
            const char* conversionSql=sql.c_str() ;
            OperationBDD(conversionSql);
            
            
            break;
        }
        case 5 :
        {
            cin.clear();
            cin.ignore();
            string studio;
            cout << "Entrez le nouveau studio" << endl;
            getline(cin,studio);
            
            
            string sql="UPDATE DVD SET STUDIO='"+studio+"' WHERE ID='"+idDVDamodifier+"' ";
            const char* conversionSql=sql.c_str() ;
            OperationBDD(conversionSql);
            break;
        }
        case 6 :
        {
            cin.clear();
            cin.ignore();
            string genre1, genre2;
            cout << "Merci de redefinir le genre, avec un maximum de deux attributs" << endl;
            getline(cin,genre1);
            getline(cin,genre2);
            
            
            string sql="UPDATE DVD SET GENRE_1='"+genre1+"' WHERE ID='"+idDVDamodifier+"' ";
            const char* conversionSql=sql.c_str() ;
            OperationBDD(conversionSql);
            
            string sql2="UPDATE DVD SET GENRE_2='"+genre2+"' WHERE ID='"+idDVDamodifier+"' ";
            const char* conversionSql2=sql2.c_str() ;
            OperationBDD(conversionSql2);
            
            
            break;
        }
            
    }
    
    
}


















