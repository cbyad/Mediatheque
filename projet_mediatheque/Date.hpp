//
//  Age.hpp
//  exo
//
//  Created by CB mac  on 19/10/2015.
//  Copyright © 2015 CB mac . All rights reserved.
//

#ifndef Date_hpp
#define Date_hpp

#include <stdio.h>
#include <ctime>
#include <iostream>




class Date {
    
    int _jour , _mois ,_annee ;
    
public:
    Date();
    
    void setJour(const int jour);
    int getjour()const ;
    
    void setMois(const int mois);
    int getMois()const ;
    
    void setAnnee(const int annee);
    int getAnnee()const ;
};




#endif /* Date_hpp */
