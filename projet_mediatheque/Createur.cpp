//
//  Createur.cpp
//  projet_mediatheque
//
//  Created by CB mac  on 10/11/2015.
//  Copyright © 2015 CB mac . All rights reserved.
//

#include "Createur.hpp"
using namespace std;

std::map<std::string,Createur*>Createur:: _poolCreateur ;
unsigned int Createur::_countCreateur=0 ;


Createur::Createur(const string& nom,const string& prenom,const string& nat,const char& initiale):
Personne(nom,prenom,initiale),_nationalite(nat)

{
    addCreateur(this);
    _countCreateur++;

}

void Createur::setNationalite(const std::string& nationalite){_nationalite=nationalite;}

std::string Createur::getNationalite()const {return _nationalite;}



void Createur::ajouterStatut(const string& statut){

    _statut.push_back(statut);

}

void Createur::setStatut(std::string& statut1, std::string& statut2) {_statut[0] = statut1; _statut[1] = statut2;}

std::string Createur::getStatut(const int position) const {
    if (_statut.size()!=0 && position<_statut.size()) {
        
        return _statut[position];
    }
    string str="VIDE";
    return str ;
}



void Createur::afficher() const {

    cout << "Nom: " << getNom() << endl;
    cout << "Prenom: " << getPrenom() << endl;
    cout << "identifiant: " << getId() << endl;
    cout << "Date de Naissance: " << endl;
    cout<<this->getDate() <<endl;
    cout<<"Nationalite :"<<getNationalite()<<endl;
    afficherStatut() ;

}

void Createur::afficherStatut()const{
 
    if(_statut.size()!=0){
    for (int i=0;i<_statut.size() ; i++) {
        cout<<"statut "<<i+1<<" : "<<_statut[i]<<endl;
    }
    }
    else 
    cout<<"Aucun statut d'enregistré"<<endl;

}


Createur::~Createur(){
    deleteCreateur(this);
    _countCreateur--;
}


void Createur::deleteAllCreateur(){

_poolCreateur.clear();

}


Createur* Createur::getAdresseCreateur(const string& id){

    map<string,Createur*>::iterator it = _poolCreateur.find(id);
    
    if (it == _poolCreateur.end())
        return NULL;
    else
        return it->second;
}


void Createur::addCreateur(Createur* createur){
   
    if (createur != NULL)
        _poolCreateur[createur->getId()] = createur;//associe key id a l'@ de l'instance
  

}



void Createur::deleteCreateur(Createur* createur){
    
    if (createur != NULL)
        _poolCreateur.erase(createur->getId());
    
    
}

unsigned int Createur::getCountCreateur() {return _countCreateur ;}


//-----------------


void menuCreateur () {
    
    string continuer="OUI";
    
    int choix ;
    while (continuer=="OUI")
    {
        cin.clear();
        cin.ignore();
        
        cout << " 1. Ajouter un CREATEUR \n 2. Supprimer un CREATEUR \n 3. Modifier un CREATEUR \n 4. Obtenir une information  \n 5. Supprimer tous les CREATEURs '/!\' " << endl;
        cin >> choix;
        
        switch(choix)
        {
            case 1 :{
                cin.clear();
                cin.ignore();
                string sql ;
                Createur* createur ;
                createur=ajouterCreateur() ;
                
                if(createur!=NULL){
                sql="INSERT INTO CREATEUR (ID,NOM,PRENOM,NATIONALITE,DATE,STATUT_1,STATUT_2) " \
                "VALUES ('"+createur->getId()+"','"+createur->getNom()+"','"+createur->getPrenom()+"','"+createur->getNationalite()+"','"+createur->getDate()+"',"\
                "'"+createur->getStatut(0)+"','"+createur->getStatut(1)+"'); ";
                
                const char* conversionSql=sql.c_str() ;
                    OperationBDD(conversionSql); //ajout de l'info dans mediatheque.db
                
                }
                else cout<<"ajout de l'information impossible dans la base de donnee"<<endl ;
                break;
             
            }
                
            case 2 : {
                cin.clear();
                cin.ignore();
                string idCDaSupprimer ;
                OperationBDD("SELECT ID,NOM,PRENOM from CREATEUR");
                cout<<"saisir ID du CREATEUR a supprimer:  \t" ;
                getline(cin,idCDaSupprimer);
                Createur* createur =Createur::getAdresseCreateur(idCDaSupprimer);
                
                string sql= "DELETE from CREATEUR where ID='"+idCDaSupprimer+"'" ;
                
                const char* conversionSql=sql.c_str() ;
                
                if (createur==NULL) {
                    
                    OperationBDD(conversionSql);     //supprimer bdd
                    
                }
                else {
                    Createur::deleteCreateur(createur) ;//supprimer du pool
                    OperationBDD(conversionSql);    //supprimer bdd
                    
                }
                break ;
            }
            case 3:{
                
                cin.clear();
                cin.ignore();
                string idCREATEURamodifier ;
                OperationBDD("SELECT ID,NOM,PRENOM from CREATEUR");
                cout<<"Saisissez l'id du CREATEUR a modifier \t";
                getline(cin,idCREATEURamodifier);
                Createur* createur = Createur::getAdresseCreateur(idCREATEURamodifier);
                if (createur == NULL) {
                    cout << "Vous souhaitez modifier ..." << endl;
                    cout << "1. Le nom\n2. Le Prenom\n3. La nationalite\n4. La date de naissance\n5. Le statut" << endl;
                    cin >> choix;
                    ChoixmodifCreateurBDD(choix,idCREATEURamodifier);
                    
                    
                }
                else{
                    cout << "Vous souhaitez modifier ..." << endl;
                    cout << "1. Le nom\n2. Le Prenom\n3. La nationalite\n4. La date de naissance\n5. Le statut" << endl;
                    cin >> choix;
                    ChoixmodifCreateur(choix,createur);
                }
                
                
                break ;
            }
            case 4:{
                cin.clear();
                cin.ignore();
                string idCREATEURaAfficher ;
                OperationBDD("SELECT ID,NOM,PRENOM from CREATEUR");
                cout<<"saisir ID du CREATEUR a afficher:  \t" ;
                getline(cin,idCREATEURaAfficher);
                
                string sql= "SELECT * from CREATEUR where ID='"+idCREATEURaAfficher+"';" ;//
                
                const char* conversionSql=sql.c_str() ;
                OperationBDD(conversionSql);
                break ;
                
            }
            case 5:{
                Createur::deleteAllCreateur() ;
                OperationBDD("DELETE  from CREATEUR ;");
                break ;
            }
                
            default:
                
                break ;
                
                
        }
        cout << "Souhaitez vous avoir une autre action sur un CREATEUR ? oui/non \t" ;
        cin >> continuer;
        majuscule(continuer);
    }
    
}



Createur* ajouterCreateur() {
    //-------------
    cin.clear();
   
    
    //-------------
    int jour , mois ,annee ;
    string statut; string reponse ;// pour ajouter un autre statut de createur
    string nom ; string prenom ; string nationalite ;
    
 
   
    //------------
    
    cout<<"saisir le nom : \t ";
    getline(cin,nom);
    cout<<"saisir la prenom : \t";
    getline(cin,prenom);
    cout<<"saisir la nationalite : \t";
    getline(cin,nationalite);
    

 
    Createur* createur=new Createur(nom,prenom,nationalite);
    
    cout<<"saisir la date de naissance du createur"<<endl ;
    cout<<"le jour : \t";cin>>jour ;
    cout<<"le mois : \t";cin>>mois ;
    cout<<"l'annee : \t";cin>>annee ;
   
    
    createur->setDate(jour, mois, annee);
    cin.clear();
    cin.ignore();


    cout<<"ajoutez un statut particulier du createur \t" ;
    getline(cin,statut);
    createur->ajouterStatut(statut);
    
    cout<<"souhaitez vous ajoutez un autre statut ? oui/non";
    
    getline(cin,reponse);
    majuscule(reponse);
    
    if (reponse=="OUI") {
        cout<<"saisir le genre \t ";
        getline(cin,statut);
        createur->ajouterStatut(statut);
    }
    
    return createur ;
    
}



void ChoixmodifCreateur(int choix, Createur* createur_a_modifier) //Implémenter les modif dans la base de donnée.
{
    switch(choix){
            
        case 1 :
        {
            cin.clear();
            cin.ignore();
            string nom;
            cout << "Saisir le nom" << endl;
            getline(cin,nom);
            createur_a_modifier->setNom(nom);
            
            string sql="UPDATE CREATEUR SET NOM='"+nom+"' WHERE ID='"+createur_a_modifier->getId()+"' ";
            const char* conversionSql=sql.c_str() ;
            OperationBDD(conversionSql);
            
            break;
        }
        case 2 :
        {
            cin.clear();
            cin.ignore();
            string prenom;
            cout << "Saisir le prenom" << endl;
            getline(cin,prenom);
            createur_a_modifier->setPrenom(prenom);
            
            string sql="UPDATE CREATEUR SET PRENOM='"+prenom+"' WHERE ID='"+createur_a_modifier->getId()+"' ";
            const char* conversionSql=sql.c_str() ;
            OperationBDD(conversionSql);
            
            break;
        }
        case 3 :
        {
            cin.clear();
            cin.ignore();
            string nationalite;
            cout << "Saisir la nationalite" << endl;
            getline(cin,nationalite);
            createur_a_modifier->setNationalite(nationalite);
            
            string sql="UPDATE CREATEUR SET NATIONALITE='"+nationalite+"' WHERE ID='"+createur_a_modifier->getId()+"' ";
            const char* conversionSql=sql.c_str() ;
            OperationBDD(conversionSql);
            
            break;
        }
        case 4 :
        {
            cin.clear();
            cin.ignore();
            int jour,mois,annee;
            cout << "Saisir la nouvelle date de creation du createur :" << endl ;
            cout << "Le jour : \t " ;cin >> jour ;
            cout << "Le mois : \t"; cin >> mois ;
            cout << "L'annee : \t";cin >> annee ;
            createur_a_modifier->setDate(jour, mois, annee);
            string date=createur_a_modifier->getDate();
            
            string sql="UPDATE CREATEUR SET DATE='"+date+"' WHERE ID='"+createur_a_modifier->getId()+"'";
            const char* conversionSql=sql.c_str() ;
            OperationBDD(conversionSql);
            
            break;
        }
       
        case 5 :
        {
            cin.clear();
            cin.ignore();
            string statut1, statut2;
            cout << "Merci de redefinir le statut, avec un maximum de deux attributs" << endl;
            getline(cin,statut1);
            getline(cin,statut2);
            
            createur_a_modifier->setStatut(statut1, statut2);
            
            string sql="UPDATE CREATEUR SET STATUT_1='"+statut1+"' WHERE ID='"+createur_a_modifier->getId()+"' ";
            const char* conversionSql=sql.c_str() ;
            OperationBDD(conversionSql);
            
            string sql2="UPDATE CREATEUR SET STATUT_2='"+statut2+"' WHERE ID='"+createur_a_modifier->getId()+"' ";
            const char* conversionSql2=sql2.c_str() ;
            OperationBDD(conversionSql2);
            
            
            break;
        }
            
    }
}


void ChoixmodifCreateurBDD(int choix,const string& idCREATEURamodifier ){
    
    
    switch(choix){
        case 1 :
        {
            
            
            cin.clear();
            cin.ignore();
            string nom;
            cout << "Saisir le nom" << endl;
            getline(cin,nom);
            
            
            string sql="UPDATE CREATEUR SET NOM='"+nom+"' WHERE ID='"+idCREATEURamodifier+"' ";
            const char* conversionSql=sql.c_str() ;
            OperationBDD(conversionSql);
            break;
        }
            
        case 2 :
        {
            cin.clear();
            cin.ignore();
            string prenom;
            cout << "Saisir le prenom" << endl;
            getline(cin,prenom);
           
            string sql="UPDATE CREATEUR SET PRENOM='"+prenom+"' WHERE ID='"+idCREATEURamodifier+"' ";
            const char* conversionSql=sql.c_str() ;
            OperationBDD(conversionSql);
            
            break;
        }
        case 3 :
        {
            cin.clear();
            cin.ignore();
            string nationalite;
            cout << "Saisir la nationalite" << endl;
            getline(cin,nationalite);
        
            
            string sql="UPDATE CREATEUR SET NATIONALITE='"+nationalite+"' WHERE ID='"+idCREATEURamodifier+"' ";
            const char* conversionSql=sql.c_str() ;
            OperationBDD(conversionSql);
            break;
        }
        case 4 :
        {
            cin.clear();
            cin.ignore();
            int jour,mois,annee;
            cout << "Saisir la nouvelle date de creation du createur :" << endl ;
            cout << "Le jour : \t " ;cin >> jour ;
            cout << "Le mois : \t"; cin >> mois ;
            cout << "L'annee : \t";cin >> annee ;
            string date=int2String(jour)+"/"+int2String(mois)+"/"+int2String(annee);
            
            string sql="UPDATE CREATEUR SET DATE='"+date+"' WHERE ID='"+idCREATEURamodifier+"'";
            const char* conversionSql=sql.c_str() ;
            OperationBDD(conversionSql);
            
            
            break;
        }
        case 5 :
        {
            cin.clear();
            cin.ignore();
            string statut1, statut2;
            cout << "Merci de redefinir le statut, avec un maximum de deux attributs" << endl;
            getline(cin,statut1);
            getline(cin,statut2);
            
            string sql="UPDATE CREATEUR SET STATUT_1='"+statut1+"' WHERE ID='"+idCREATEURamodifier+"' ";
            const char* conversionSql=sql.c_str() ;
            OperationBDD(conversionSql);
            
            string sql2="UPDATE CREATEUR SET STATUT_2='"+statut2+"' WHERE ID='"+idCREATEURamodifier+"' ";
            const char* conversionSql2=sql2.c_str() ;
            OperationBDD(conversionSql2);
            
            
            break;
            
        }
            default:
            
            break;
            
            
                }
            
    }