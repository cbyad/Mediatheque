//
//  Utilisateur.cpp
//  projet_mediatheque
//
//  Created by CB mac  on 10/11/2015.
//  Copyright © 2015 CB mac . All rights reserved.
//

#include "Utilisateur.hpp"
using namespace std;

std::map<std::string,Utilisateur*> Utilisateur:: _poolUser ;
unsigned int Utilisateur:: _countUser=0 ;


Utilisateur::Utilisateur(const std::string& nom,const std::string& prenom,const std::string& adresse,const std::string& numero,const char& initiale)
:Personne(nom,prenom,initiale),_adresse(adresse),_numeroTelephone(numero)

{
    addUser(this);
    _countUser++;
}


void Utilisateur::afficher() const {
    
    cout << "Nom: " << getNom() << endl;
    cout << "Prenom: " << getPrenom() << endl;
    cout << "Adresse: " << getAdresse() << endl;
    cout << "Numero de telephone: " << getNumeroTelephone() << endl;
    cout << "Date: " << getDate() << endl;
    
}


Utilisateur::~Utilisateur() {
    deleteUser(this);
    _countUser--;
}


void Utilisateur::deleteAllUser(){
    
    _poolUser.clear();
    
}


Utilisateur* Utilisateur::getUserAdresse(const string& id){
    
    map<string,Utilisateur*>::iterator it = _poolUser.find(id);
    
    if (it == _poolUser.end())
        return NULL;
    else
        return it->second;
}


void Utilisateur::addUser(Utilisateur* user){
    
    if (user != NULL)
        _poolUser[user->getId()] = user;//associe key id a l'@ de l'instance
    
}



void Utilisateur::deleteUser(Utilisateur* user){
    
    if (user != NULL)
        _poolUser.erase(user->getId());
    
}

void Utilisateur::setNumeroTelephone(const string& tel){_numeroTelephone=tel ;}


string Utilisateur::getNumeroTelephone()const {return _numeroTelephone;}

void Utilisateur::setAdresse(const string& adr){ _adresse=adr ;}


string Utilisateur::getAdresse()const {return _adresse ;}


void Utilisateur::setListeArticles(Article* article){// cette methode remplit la list avec l'@ de l'objet emprunté
    
    _articleEmpruntes.push_back(article);
}



//------------------


void menuUtilisateur() {
    string continuer = "OUI";
    
    int choix ;
    while (continuer=="OUI")
    {
        cin.clear();
        cin.ignore();
        
        cout << " 1. Ajouter un Utilisateur \n 2. Supprimer un Utilisateur \n 3. Modifier un Utilisateur \n 4. Obtenir une information  \n 5. Supprimer tous les Utilisateurs '/!\\' " << endl;
        cin >> choix;
        
        switch(choix)
        {
            case 1 :{
                cin.clear();
                cin.ignore();
                string sql ;
                Utilisateur* user ;
                user = ajouterUtilisateur() ;
                
                
                if(user != NULL){
                    sql="INSERT INTO UTILISATEUR (ID,NOM,PRENOM,DATE_NAISSANCE,NUMERO_TELEPHONE,ADRESSE) " \
                    "VALUES ('"+user->getId()+"','"+user->getNom()+"','"+user->getPrenom()+"','"+user->getDate()+"',"\
                    "'"+user->getNumeroTelephone()+"','"+user->getAdresse()+"') ";
                    const char* conversionSql = sql.c_str() ;
                    
                    
                    OperationBDD(conversionSql); //ajout de l'info dans mediatheque.db
                }
                else
                    cout << "Ajout de l'information impossible dans la base de donnee" << endl ;
                break;
                
            }
                
            case 2 : {
                cin.clear();
                cin.ignore();
                string idUseraSupprimer ;
                OperationBDD("SELECT ID,NOM,PRENOM from UTILISATEUR");
                cout << "Saisissez l'ID de l'utilisateur a supprimer: \t" ;
                getline(cin,idUseraSupprimer);
                Utilisateur* user = Utilisateur::getUserAdresse(idUseraSupprimer);
                
                string sql= "DELETE from UTILISATEUR WHERE ID='"+idUseraSupprimer+"'" ;
                
                const char* conversionSql = sql.c_str() ;
                if (user == NULL) {
                    
                    OperationBDD(conversionSql);     //supprimer bdd
                    
                }
                else {
                    Utilisateur::deleteUser(user) ;//supprimer du pool
                    OperationBDD(conversionSql);    //supprimer bdd
                    
                }
                
                break ;
            }
            case 3:{
                cin.clear();
                cin.ignore();
                string idUseramodifier ;
                OperationBDD("SELECT ID,NOM,PRENOM from UTILISATEUR");
                cout<<"Saisissez l'ID de l'utilisateur a modifier \t";
                getline(cin,idUseramodifier);
                Utilisateur* user = Utilisateur::getUserAdresse(idUseramodifier);
                if (user == NULL) {
                    cout << "Vous souhaitez modifier ..." << endl;
                    cout << "1. Le nom\n2. Le prenom\n3. La date de naissance\n4. Le numero de telephone\n5. L'adresse\n" << endl;
                    cin >> choix;
                    ChoixmodifUserBDD(choix,idUseramodifier);
                }
                else{
                    cout << "Vous souhaitez modifier ..." << endl;
                    cout << "1. Le nom\n2. Le prenom\n3. La date de naissance\n4. Le numero de telephone\n5. L'adresse\n" << endl;
                    cin >> choix;
                    ChoixmodifUser(choix,user);
                }
                
                break ;
            }
            case 4:{
                cin.clear();
                cin.ignore();
                string idUtilisateuraAfficher ;
                OperationBDD("SELECT ID,NOM,PRENOM from UTILISATEUR");
                cout << "Saisissez ID de l'utilisateur a afficher:  \t" ;
                getline(cin,idUtilisateuraAfficher);
                
                string sql = "SELECT * from UTILISATEUR where ID='"+idUtilisateuraAfficher+"';" ;//
                
                const char* conversionSql = sql.c_str() ;
                OperationBDD(conversionSql);
                break ;
            }
            case 5:{
                Utilisateur::deleteAllUser() ;
                OperationBDD("DELETE  FROM  UTILISATEUR ;");
                break ;
            }
                
            default:
                cout << "Commande entree invalides" << endl;
                break ;
        }
        cout << "Souhaitez vous avoir une autre action sur un Utilsateur ? oui/non" << endl;
        cin >> continuer;
        majuscule(continuer);
    }
    
}


Utilisateur* ajouterUtilisateur() {
    
    //-------------
    cin.clear();
    // cin.ignore();
    //--------------
    
    int jour , mois ,annee ;
    string nom; string adresse ;
    string idUtilisateur ; string prenom ; string numTel ;
    string datenaissance;string articleEmprunte;
    
    cout << "Saississez le nom" << endl;
    getline(cin,nom);
    
    cout<<"Saississez le prenom" << endl;
    getline(cin,prenom);
    
    cout<<"Saississez son adresse" << endl;
    getline(cin,adresse);
    
    cout << "Saississez son numero de telephone" << endl;
    
    Utilisateur* user = new Utilisateur(nom,prenom,adresse,numTel);
    
    cout<<"saisissez la date de naissance"<<endl ;
    cout<<"Le jour : \t";
    cin>>jour ;
    cout<<"Le mois : \t";
    cin>>mois ;
    cout<<"L'annee : \t";
    cin>>annee ;
    user->setDate(jour, mois, annee);
    
    cin.clear();
    cin.ignore();
    
 
    return user ;
    
}


void ChoixmodifUser(int choix,Utilisateur* user_a_modifier)
{
    switch(choix){
            
        case 1 :
        {
            cin.clear();
            cin.ignore();
            string nom;
            cout << "Saisissez le nom" << endl;
            getline(cin,nom);
            user_a_modifier->setNom(nom);
            
            string sql = "UPDATE UTILISATEUR SET NOM='"+nom+"' WHERE ID='"+user_a_modifier->getId()+"' ";
            const char* conversionSql = sql.c_str() ;
            OperationBDD(conversionSql);
            
            break;
        }
        case 2 :
        {
            cin.clear();
            cin.ignore();
            string prenom;
            cout << "Saisissez le prenom" << endl;
            getline(cin,prenom);
            user_a_modifier->setPrenom(prenom);
            
            string sql = "UPDATE UTILISATEUR SET PRENOM='"+prenom+"' WHERE ID='"+user_a_modifier->getId()+"' ";
            const char* conversionSql = sql.c_str() ;
            OperationBDD(conversionSql);
            
            break;
        }
        case 3 :
        {
            cin.clear();
            cin.ignore();
            int jour,mois,annee;
            cout << "Saisissez la nouvelle date de naissance :" << endl ;
            cout << "Le jour : \t " ;cin >> jour ;
            cout << "Le mois : \t"; cin >> mois ;
            cout << "L'annee : \t";cin >> annee ;
            user_a_modifier->setDate(jour, mois, annee);
            string date = user_a_modifier->getDate();
            
            string sql="UPDATE UTILISATEUR SET DATE_NAISSANCE='"+date+"' WHERE ID='"+user_a_modifier->getId()+"'";
            const char* conversionSql = sql.c_str() ;
            OperationBDD(conversionSql);
            
            break;
        }
        case 4 :
        {
            cin.clear();
            cin.ignore();
            string numero;
            cout << "Entrez le nouveau numero de telephone" << endl;
            cin >> numero;
            user_a_modifier->setNumeroTelephone(numero);
            
            string sql = "UPDATE UTILISATEUR SET NUMERO_TELEPHONE="+user_a_modifier->getNumeroTelephone()+" WHERE ID='"+user_a_modifier->getId()+"' ";
            const char* conversionSql = sql.c_str() ;
            OperationBDD(conversionSql);
            
            break;
        }
        case 5 :
        {
            cin.clear();
            cin.ignore();
            string adresse;
            cout << "Entrez la nouvelle adresse" << endl;
            getline(cin,adresse);
            user_a_modifier->setAdresse(adresse);
            
            string sql="UPDATE UTILISATEUR SET ADRESSE='"+adresse+"' WHERE ID='"+user_a_modifier->getId()+"' ";
            const char* conversionSql = sql.c_str() ;
            OperationBDD(conversionSql);
            
            break;
        }
        default :
        {
            cout << "Commande entree invalide" << endl;
            break;
        }
            
    }
}


void ChoixmodifUserBDD(int choix,const std::string& idUseramodifier)
{
    switch(choix){
        case 1 :
        {
            cin.clear();
            cin.ignore();
            string nom;
            cout << "Saisissez le nom" << endl;
            getline(cin,nom);
            
            string sql="UPDATE UTILISATEUR SET NOM='"+nom+"' WHERE ID='"+idUseramodifier+"' ";
            const char* conversionSql = sql.c_str() ;
            OperationBDD(conversionSql);
            
            break;
        }
        case 2 :
        {
            cin.clear();
            cin.ignore();
            string prenom;
            cout << "Saisissez le prenom" << endl;
            getline(cin,prenom);
            
            string sql = "UPDATE UTILISATEUR SET PRENOM='"+prenom+"' WHERE ID='"+idUseramodifier+"' ";
            const char* conversionSql = sql.c_str() ;
            OperationBDD(conversionSql);
            
            break;
        }
        case 3 :
        {
            cin.clear();
            cin.ignore();
            int jour,mois,annee;
            cout << "Saisissez la nouvelle date de naissance :" << endl ;
            cout << "Le jour : \t " ;cin >> jour ;
            cout << "Le mois : \t"; cin >> mois ;
            cout << "L'annee : \t";cin >> annee ;
            string date=int2String(jour)+"/"+int2String(mois)+"/"+int2String(annee);
            string sql="UPDATE UTILISATEUR SET DATE_NAISSANCE='"+date+"' WHERE ID='"+idUseramodifier+"'";
            const char* conversionSql = sql.c_str() ;
            OperationBDD(conversionSql);
            
            break;
        }
        case 4 :
        {
            cin.clear();
            cin.ignore();
            string numero;
            cout << "Entrez le nouveau numero de telephone" << endl;
            cin >> numero;
            
            string sql = "UPDATE UTILISATEUR SET NUMERO_TELEPHONE="+numero+" WHERE ID='"+idUseramodifier+"' ";
            const char* conversionSql = sql.c_str() ;
            OperationBDD(conversionSql);
    
            break;
        }
        case 5 :
        {
            cin.clear();
            cin.ignore();
            string adresse;
            cout << "Entrez la nouvelle adresse" << endl;
            getline(cin,adresse);
            
            string sql="UPDATE UTILISATEUR SET ADRESSE='"+adresse+"' WHERE ID='"+idUseramodifier+"' ";
            const char* conversionSql = sql.c_str() ;
            OperationBDD(conversionSql);
            
            
            break;
        }
        default :
        {
            cout << "Commande entree invalide" << endl;
            break;
        }
    }
}
    