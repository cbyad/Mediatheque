//
//  Date.cpp
//  exo
//
//  Created by CB mac  on 19/10/2015.
//  Copyright © 2015 CB mac . All rights reserved.
//
#include "Date.hpp"
using namespace std;


Date::Date(){
    
    time_t actuel = time(0);
    tm *ptr_tm = localtime(&actuel);
    
    _jour=ptr_tm->tm_mday ;
     _mois=ptr_tm->tm_mon;
     _annee=(1900 + ptr_tm->tm_year ) ;

}

void Date::setJour(const int jour ){
    _jour=jour ;
}

int Date::getjour()const{
    return _jour ;
    
}



void Date::setMois(const int mois ){
    _mois=mois ;
}

int Date::getMois()const{
    return _mois ;
    
}

void Date::setAnnee(const int annee ){
    _annee=annee ;
}

int Date::getAnnee()const{
    return _annee ;
    
}


