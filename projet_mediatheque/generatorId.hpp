//
//  generatorId.hpp
//  projet_mediatheque
//
//  Created by CB mac  on 08/12/2015.
//  Copyright © 2015 CB mac . All rights reserved.
//

#ifndef generatorId_hpp
#define generatorId_hpp

#include <stdio.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <sqlite3.h>



void OperationBDD(const char* );

std::string generer(const char& , int );

std::string int2String(int);
int recupererId();


void majuscule(std::string& );


#endif /* generatorId_hpp */
