//
//  Livre.hpp
//  projet_mediatheque
//
//  Created by CB mac  on 16/11/2015.
//  Copyright © 2015 CB mac . All rights reserved.
//

#ifndef Livre_hpp
#define Livre_hpp

#include <stdio.h>
#include <map>
#include "Article.hpp"

class Livre: public Article{
    
private:
    std::string  _editeur ;
    
    static std::map<std::string,Livre*> _poolBook ;
    static unsigned int _countBook ;
    
public:
    Livre(const std::string& titre="",const std::string& editeur="",const int nombrepages=0,const Disponibilite& dispo=DISPONIBLE,const char& initiale='L') ;
  
    virtual void afficher() const ;
    void setEditeur(const std::string& );
    std::string getEditeur() const ;
    
    virtual std::string getStudio()const ;
    
    virtual ~Livre() ;
    static unsigned int getCountBook();
    
    static void deleteAllBook(); // libère tous les Livres du pool
    static Livre* getBookAdresse(const std::string& );
    static void addBoook(Livre* livre); // enregistre un livre dans le pool
    static void deleteBook(Livre *livre);
    
};
//-------------
void menuLivre() ;
Livre* ajouterLivre() ;
void ChoixmodifLivre(int choix, Livre* livre_a_modifier);
void ChoixmodifLivreBDD(int choix,const std::string& idDLIVREamodifier );

//------------

#endif /* Livre_hpp */
