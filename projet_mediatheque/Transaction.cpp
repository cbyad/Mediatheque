
//
//  Transaction.cpp
//  projet_mediatheque
//
//  Created by CB mac  on 16/11/2015.
//  Copyright © 2015 CB mac . All rights reserved.
//

#include "Transaction.hpp"




using namespace std;
//----------------

unsigned int Transaction::_count = 0;

map<string,Transaction*> Transaction::_poolTransaction ;

//-----------------

unsigned int Transaction::getTransactionCount()
{
    return _count;
}


Transaction::Transaction(const Etat& etat ,const char& initiale):_articlePrete(NULL),_emprunteur(NULL),_etat(etat)
{
    
    addTransaction(this);
    Transaction::_count++;
    _id=generer(initiale, recupererId());
}



Transaction::~Transaction() {
    deleteTransaction(this);
    delete _articlePrete ;
    delete _emprunteur;
    _count--;
    
}


void Transaction::setEtat(const Etat& etat)
{
    _etat=etat ;
}

Etat Transaction::getEtat() const
{
    return _etat ;
}


string Transaction::getId() const
{
    return _id;
}


void Transaction::setDatePret(const int jour, const int mois , const int annee)
{
    _datePret.setJour(jour);
    _datePret.setMois(mois);
    _datePret.setAnnee(annee);
}


string Transaction::getDatePret() const

{
    string str ;
    int j=_datePret.getjour() , m=_datePret.getMois() ,a=_datePret.getAnnee() ;
    
    
    str=int2String(j)+"/"+int2String(m)+"/"+int2String(a);
    return str ;
    
}


void Transaction::setDateRendu(const int jour, const int mois , const int annee)
{
    _dateRendu.setJour(jour);
    _dateRendu.setMois(mois);
    _dateRendu.setAnnee(annee);
    
}

string Transaction::getDateRendu() const
{
    string str ;
    int j=_dateRendu.getjour() , m=_dateRendu.getMois() ,a=_dateRendu.getAnnee() ;
    
    
    str=int2String(j)+"/"+int2String(m)+"/"+int2String(a);
    return str ;
}

Article* Transaction::getArticleEmprunte() const {return _articlePrete ;}
Utilisateur* Transaction::getEmprunteur() const {return  _emprunteur ;}








void Transaction::addTransaction(Transaction* transaction){
    if(transaction!=NULL) _poolTransaction[transaction->getId()]=transaction;
    
}


void Transaction::deleteTransaction(Transaction* transaction){
    if(transaction!=NULL) _poolTransaction.erase(transaction->getId());
    
}


void Transaction::deleteAllTransaction(){
    _poolTransaction.clear();
}


Transaction* Transaction::getTransactionAdresse(const string& id){
    
    map<string,Transaction*> :: iterator it=_poolTransaction.find(id);
    
    if(it==_poolTransaction.end()) return NULL ;
    return it->second ;
    
}


//--------------

void Transaction::pretLivre(const std::string& idBook ,const string& idUser ){
    
    Utilisateur* user = NULL;
    Livre* book = NULL ;
    book = Livre::getBookAdresse(idBook);
    user = Utilisateur::getUserAdresse(idUser);
    
    if (book != NULL && user != NULL) {
        
        if (book->getDisponibilite() == DISPONIBLE) {
            
            _emprunteur = user;
            _articlePrete = book ;
            _emprunteur->setListeArticles(book);
            book->setDisponibilite(EMPRUNTE);
            _etat = EN_COURS;
            //modd dans la bdd
        }
        else cout << "Livre non disponible" << endl ;
    }
    cout << "Livre non existant/Utilisateur non existant " << endl ;
    
    
}



void Transaction::pretCd( const std::string& idCd,const std::string& idUser){
    Utilisateur* user=NULL ;
    Cd* cd = NULL ;
    cd = Cd::getCdAdresse(idCd);
    user = Utilisateur::getUserAdresse(idUser);
    
    if (cd != NULL && user != NULL) {
        if (cd->getDisponibilite() == DISPONIBLE) {//
            
            _emprunteur = user;
            _articlePrete = cd ;
            _emprunteur->setListeArticles(cd);
            cd->setDisponibilite(EMPRUNTE);
            _etat = EN_COURS;
            //mod dans la bdd
        }
        else cout << "Cd non disponible" << endl ;
    }
    cout << "Cd non existant/Utilisateur non existant " << endl ;
    
    
}

void Transaction::pretDvd( const std::string& idDvd,const std::string& idUser){
    Utilisateur* user = NULL ;
    Dvd* dvd = NULL ;
    dvd = Dvd::getDvdAdresse(idDvd);
    user = Utilisateur::getUserAdresse(idUser);
    
    if (dvd != NULL && user != NULL) {
        if (dvd->getDisponibilite() == DISPONIBLE) {//
            _emprunteur = user;
            _articlePrete = dvd ;
            _emprunteur->setListeArticles(dvd);
            dvd->setDisponibilite(EMPRUNTE);
            
            //mod dans la bdd
        }
        else cout << "Dvd non disponible" << endl ;
    }
    cout << "Dvd non existant " << endl ;
    
    
}

//-------------------------------


void menuTransaction() {
    
    string continuer="OUI";
    
    int choix ;
    while (continuer=="OUI")
    {
        
        cout << " 1. Ajouter une TRANSACTION \n 2. Supprimer une TRANSACTION \n 3. Modifier une TRANSACTION \n 4. Obtenir une information  \n 5. Supprimer toutes les TRANSACTIONs '/!\' " << endl;
        cin >> choix;
        
        switch(choix)
        {
            case 1 :{
                string sql ;
                Transaction* transaction ;
                transaction = ajouterTransaction() ;
                
                if (transaction != NULL) {
                    
                    if(transaction->getArticleEmprunte() == NULL || transaction->getEmprunteur() == NULL){ // alors aucun sens de parler de transaction
                        transaction->setDatePret(0, 0, 0);
                        transaction->setDateRendu(0, 0, 0);
                        sql = "INSERT INTO TRANSACTIONS (ID,ETAT,DATE_PRET,DATE_RENDU,ARTICLE,UTILISATEUR) " \
                        "VALUES ('"+transaction->getId()+"','"+int2String(transaction->getEtat())+"','AUCUNE','AUCUNE','INCONNU',"\
                        "'INCONNU'); ";
                        
                    }
                    else{
                        sql = "INSERT INTO TRANSACTIONS(ID,ETAT,DATE_PRET,DATE_RENDU,ARTICLE,UTILISATEUR) " \
                        "VALUES ('"+transaction->getId()+"','"+int2String(transaction->getEtat())+"','"+transaction->getDatePret()+"','"+transaction->getDateRendu()+"','"+transaction->getArticleEmprunte()->getId()+"',"\
                        "'"+transaction->getEmprunteur()->getId()+"'); ";
                        
                        
                    }
                    
                    const char* conversionSql = sql.c_str() ;
                    
                    
                    OperationBDD(conversionSql); //ajout de l'info dans mediatheque.db
                }
                
                else cout << "Ajout de l'information impossible dans la base de donnee" << endl ;
                break;
                
            }
                
            case 2 : {
                string idTRANSACTIONaSupprimer ;
                OperationBDD("SELECT ID,ETAT,ARTICLE,UTILISATEUR from TRANSACTIONS");
                cout<<"Saisissez l'ID de la TRANSACTION  a supprimer:  \t" ;
                getline(cin,idTRANSACTIONaSupprimer);
                Transaction* transaction = Transaction::getTransactionAdresse(idTRANSACTIONaSupprimer);
                
                string sql= "DELETE from TRANSACTIONS where ID="+idTRANSACTIONaSupprimer+";" ;
                
                const char* conversionSql = sql.c_str() ;
                
                if (transaction == NULL) {
                    
                    OperationBDD(conversionSql);     //supprimer bdd
                    
                }
                else {
                    Transaction::deleteTransaction(transaction) ;//supprimer du pool
                    OperationBDD(conversionSql);    //supprimer bdd
                    
                }
                
                break ;
            }
            case 3:{
                string idTRANSACTIONaModifier ;
                OperationBDD("SELECT ID,ETAT,ARTICLE,UTILISATEUR from TRANSACTIONS");
                cout << "Saisissez l'ID de la TRANSACTION  a modifier:  \t" ;
                getline(cin,idTRANSACTIONaModifier);
                
                Transaction* transac = Transaction::getTransactionAdresse(idTRANSACTIONaModifier);
                
                if (transac == NULL) {
                    cout << "Vous souhaitez modifier ..." << endl;
                    cout << "1. L'etat\n2. La date de pret\n3. La date de rendu\n4. L'article et l'utilisateur " << endl;
                    cin >> choix;
                    ChoixmodifTransactionBDD(choix,idTRANSACTIONaModifier);
                }
                else{
                    cout << "Vous souhaitez modifier ..." << endl;
                    cout << "1. L'etat\n2. La date de pret\n3. La date de rendu\n4. L'article et l'utilisateur " << endl;
                    cin >> choix;
                    ChoixmodifTransaction(choix,transac);
                }
                
                break ;
            }
            case 4:{
                string idTransactionaAfficher ;
                OperationBDD("SELECT ID,NOM,PRENOM from TRANSACTIONS");
                cout<<"Saisir ID du de la transaction a afficher:  \t" ;
                getline(cin,idTransactionaAfficher);
                
                string sql= "SELECT * from TRANSACTIONS where ID='"+idTransactionaAfficher+"';" ;//
                
                const char* conversionSql = sql.c_str() ;
                OperationBDD(conversionSql);
                break ;
            }
            case 5:{
                Transaction::deleteAllTransaction() ;
                OperationBDD("DELETE * from TRANSACTIONS ;");
                break ;
            }
                
            default:
                
                break ;
        }
        cout << "Souhaitez vous avoir une autre action sur une Transaction ? oui/non" << endl;
        cin >> continuer;
        majuscule(continuer);
    }
}

Transaction* ajouterTransaction() {
    
    //-------------
    cin.clear();
    // cin.ignore();
    //--------------
    
    //-------------
    int jourA , moisA ,anneeA ; //date pret
    int jourB , moisB ,anneeB ; //date rendu
    
    string idUtilisateur ; string idArticle ;
    int choixArticle;
    
    //------------
    
    cout << "Saisissez la date de pret" << endl;
    
    cout << "Le jour : \t";cin >> jourA ;
    cout << "Le mois : \t";cin >> moisA ;
    cout << "L'annee : \t";cin >> anneeA ;
    
    cout << "Saisissez la date de rendu" << endl;
    
    cout << "Le jour : \t";cin >> jourB ;
    cout << "Le mois : \t";cin >> moisB ;
    cout << "L'annee : \t";cin >> anneeB;
    
    cin.ignore() ;
    
    Transaction* transaction = new Transaction();
    transaction->setDatePret(jourA,moisA, anneeA);
    transaction->setDateRendu(jourB, moisB, anneeB);
    
    
    cout<<"Saisissez l'identifiant de l'article a preter"<<endl;
    getline(cin,idArticle);
    
    cout<<"Saisissez l'identifiant de l'emprunteur "<<endl;
    getline(cin,idUtilisateur);
    
    int quit = 0;
    do{
        cout << "Quelle est la nature de l'article : 1.CD  2.DVD  3.LIVRE \t";
        cin >> choixArticle;
        
        if (choixArticle == 1) {
            quit = 1;
            transaction->pretCd(idArticle, idUtilisateur);//meilleur des cas existe dans le pool
        }
        
        if (choixArticle == 2) {
            quit = 2;
            transaction->pretDvd(idArticle, idUtilisateur);//
            
        }
        
        if (choixArticle == 3) {
            quit = 3;
            transaction->pretLivre(idArticle, idUtilisateur);//
        }
        
    }while (quit == 0);
    
    
    return transaction ;
    
    
}

void ChoixmodifTransaction(int choix,Transaction* transac_a_modifier)
{
    switch(choix){
            
        case 1 :
        {
            cin.clear();
            cin.ignore();
            Etat etat; string _etat ;
            cout << "Saisissez l'etat" << endl;
            getline(cin,_etat);
            majuscule(_etat);
            if(_etat=="AUCUNE") etat=AUCUNE ;
              if(_etat=="EN_COURS") etat=EN_COURS ;
              if(_etat=="RETARD") etat=RETARD ;
              if(_etat=="RENDU") etat=RENDU ;
              if(_etat=="FINALISE") etat=FINALISE ;
            
            
            transac_a_modifier->setEtat(etat);
            
            string sql="UPDATE TRANSACTIONS SET ETAT='"+_etat+"' WHERE ID='"+transac_a_modifier->getId()+"' ";
            const char* conversionSql = sql.c_str() ;
            OperationBDD(conversionSql);
            
            break;
        }
        case 2 :
        {
            cin.clear();
            cin.ignore();
            int jour,mois,annee;
            cout << "Saisir la nouvelle date de pret:" << endl ;
            cout << "Le jour : \t " ;cin >> jour ;
            cout << "Le mois : \t"; cin >> mois ;
            cout << "L'annee : \t";cin >> annee ;
            transac_a_modifier->setDatePret(jour, mois, annee);
            string date = transac_a_modifier->getDatePret();
            
            string sql = "UPDATE TRANSACTIONS SET DATE_PRET='"+date+"' WHERE ID='"+transac_a_modifier->getId()+"'";
            const char* conversionSql = sql.c_str() ;
            OperationBDD(conversionSql);
            
            break;
        }
        case 3 :
        {
            cin.clear();
            cin.ignore();
            int jour,mois,annee;
            cout << "Saisissez la nouvelle date de rendu:" << endl ;
            cout << "Le jour : \t " ;cin >> jour ;
            cout << "Le mois : \t"; cin >> mois ;
            cout << "L'annee : \t";cin >> annee ;
            transac_a_modifier->setDateRendu(jour, mois, annee);
            string date = transac_a_modifier->getDateRendu();
            
            string sql = "UPDATE TRANSACTIONS SET DATE_RENDU='"+date+"' WHERE ID='"+transac_a_modifier->getId()+"'";
            const char* conversionSql = sql.c_str() ;
            OperationBDD(conversionSql);
        }
        case 4 :
        {
            cin.clear();
            cin.ignore();
            string idArticle;
            cout << "Entrez l'ID de l'article" << endl;
            getline(cin,idArticle);
            
            string idUser;
            cout << "Entrez l'ID de l'emprunteur" << endl;
            getline(cin,idUser);
            
            int quit = 0;
            int choixArticle;
            do{
                cout << "Quelle est la nature de l'article : 1.CD  2.DVD  3.LIVRE \t";
                cin >> choixArticle;
                
                if (choixArticle == 1) {
                    quit = 1;
                    transac_a_modifier->pretCd(idArticle, idUser);//meilleur des cas existe dans le pool
                }
                
                if (choixArticle == 2) {
                    quit = 2;
                    transac_a_modifier->pretDvd(idArticle, idUser);//
                    
                }
                
                if (choixArticle == 3) {
                    quit = 3;
                    transac_a_modifier->pretLivre(idArticle, idUser);//
                }
                
            }while (quit == 0);
            
            string sql = "UPDATE TRANSACTIONS SET ARTICLE='"+idArticle+"' AND UTILISATEUR='"+idUser+"' WHERE ID='"+transac_a_modifier->getId()+"' ";
            const char* conversionSql = sql.c_str() ;
            OperationBDD(conversionSql);
            
            
            break;
        }

    
    default :{
    cout << "Commande saisie invalide" << endl;
    break;
}
    
}
}


void ChoixmodifTransactionBDD(int choix,const std::string& idTransactionamodifier)
{
    switch(choix){
            
        case 1 :
        {
            cin.clear();
            cin.ignore();
            Etat etat; string _etat ;
            cout << "Saisissez l'etat" << endl;
            getline(cin,_etat);
            majuscule(_etat);
            if(_etat=="AUCUNE") etat=AUCUNE ;
            if(_etat=="EN_COURS") etat=EN_COURS ;
            if(_etat=="RETARD") etat=RETARD ;
            if(_etat=="RENDU") etat=RENDU ;
            if(_etat=="FINALISE") etat=FINALISE ;

            string sql="UPDATE TRANSACTIONS SET ETAT='"+_etat+"' WHERE ID='"+idTransactionamodifier+"' ";
            const char* conversionSql = sql.c_str() ;
            OperationBDD(conversionSql);
            
            break;
        }
        case 2 :
        {
            cin.clear();
            cin.ignore();
            int jour,mois,annee;
            cout << "Saisissez la nouvelle date de pret:" << endl ;
            cout << "Le jour : \t " ;cin >> jour ;
            cout << "Le mois : \t"; cin >> mois ;
            cout << "L'annee : \t";cin >> annee ;
            string date=int2String(jour)+"/"+int2String(mois)+"/"+int2String(annee);

            string sql = "UPDATE TRANSACTIONS SET DATE_PRET='"+date+"' WHERE ID='"+idTransactionamodifier+"'";
            const char* conversionSql = sql.c_str() ;
            OperationBDD(conversionSql);
            
            break;
        }
        case 3 :
        {
            cin.clear();
            cin.ignore();
            int jour,mois,annee;
            cout << "Saisissez la nouvelle date de rendu:" << endl ;
            cout << "Le jour : \t " ;cin >> jour ;
            cout << "Le mois : \t"; cin >> mois ;
            cout << "L'annee : \t";cin >> annee ;
            string date=int2String(jour)+"/"+int2String(mois)+"/"+int2String(annee);
            string sql = "UPDATE TRANSACTIONS SET DATE_RENDU='"+date+"' WHERE ID='"+idTransactionamodifier+"'";
            const char* conversionSql = sql.c_str() ;
            OperationBDD(conversionSql);
        }
        case 4 :
        {
            cin.clear();
            cin.ignore();
            string idArticle;
            cout << "Entrez l'ID de l'article" << endl;
            getline(cin,idArticle);
            
            string idUser;
            cout << "Entrez l'ID de l'emprunteur" << endl;
            getline(cin,idUser);
            
            int quit = 0;
            int choixArticle ;
            do{
                cout << "Quelle est la nature de l'article : 1.CD  2.DVD  3.LIVRE \t";
                cin >> choixArticle;
                
                if (choixArticle == 1) {
                    quit = 1;
                    string sql = "UPDATE TRANSACTIONS SET ARTICLE='"+idArticle+"' AND UTILISATEUR='"+idUser+"' WHERE ID='"+idTransactionamodifier+"' ";
                    const char* conversionSql = sql.c_str() ;
                    OperationBDD(conversionSql);
                    
                  
                }
                
                if (choixArticle == 2) {
                    quit = 2;
                    string sql = "UPDATE TRANSACTIONS SET ARTICLE='"+idArticle+"' AND UTILISATEUR='"+idUser+"' WHERE ID='"+idTransactionamodifier+"' ";
                    const char* conversionSql = sql.c_str() ;
                    OperationBDD(conversionSql);
                    
                    
                    
                }
                
                if (choixArticle == 3) {
                    quit = 3;
                    string sql = "UPDATE TRANSACTIONS SET ARTICLE='"+idArticle+"' AND UTILISATEUR='"+idUser+"' WHERE ID='"+idTransactionamodifier+"' ";
                    const char* conversionSql = sql.c_str() ;
                    OperationBDD(conversionSql);
                    
                }
                
            }while (quit == 0);
            
            
            
            break;
        }
            
  default :{
    cout << "Commande saisie invalide" << endl;
    break;
}
    
}
}



