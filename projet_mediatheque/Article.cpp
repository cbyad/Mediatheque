//
//  Article.cpp
//  projet_mediatheque
//
//  Created by CB mac  on 10/11/2015.
//  Copyright © 2015 CB mac . All rights reserved.
//

#include "Article.hpp"
#include "Utilisateur.hpp"


using namespace std;

unsigned int Article::_count=0 ;

unsigned int Article::getArticleCount() { return _count;}



Article::Article(const string& titre ,const int duree, const Disponibilite& dispo,const char& initiale):
_titre(titre),_duree(duree),_auteur(NULL),_utilisateur(NULL),_disponibilite(dispo)

{
    
    _count++ ;
    _id=generer(initiale, recupererId());
    
}

void Article::setTitre(const std::string& titre){_titre=titre ;}

void Article::setDate(const int jour, const int mois , const int annee){
    _date.setJour(jour);
    _date.setMois(mois);
    _date.setAnnee(annee);
    
}

void Article::setGenre(std::string& genre1, std::string& genre2) {_genres[0] = genre1; _genres[1] = genre2;}

void Article::setDuree(const int duree){_duree=duree ;}


std::string Article::getTitre()const {return _titre ;}


string Article::getDate() const {
    string str ;
int j=_date.getjour() , m=_date.getMois() ,a=_date.getAnnee() ;

  
    str=int2String(j)+"/"+int2String(m)+"/"+int2String(a);
    return str ;
    
}

int Article::getDuree()const {return _duree;}

string Article::getId() const{ return _id ;}


Article::~Article()
{
    
    _count-- ;
    delete _auteur ;
    delete _utilisateur ;

}


void Article::associer(const string& idAuteur){

    Createur* adresse=NULL ;
    
    adresse=Createur::getAdresseCreateur(idAuteur);
    if (adresse!=NULL) {
        _auteur=adresse ;
    }
    else
    {
        /*
        string first="SELECT NOM FROM CREATEUR WHERE ID='"+idAuteur+"'";
        OperationBDD(first.c_str());
        
        cout<<first ;
        
        string sql="UPDATE CD SET AUTEUR='"+first+"' WHERE ID='"+_id+"' ";
        const char* conversionSql=sql.c_str() ;

        OperationBDD(conversionSql);
         */
    cout<<"identifiant auteur non existant .association impossible"<<endl ;
    }
}

Createur* Article::getAuteur()const { return _auteur;}


void Article::pretArticle(const std::string& idUser){

    if (_disponibilite==DISPONIBLE) {
        Utilisateur* user =NULL ;
        user=Utilisateur::getUserAdresse(idUser);
        if(user!=NULL) {
            _utilisateur=user ;
            _disponibilite=EMPRUNTE ;
        }
        else cout<<"utilisateur non existant "<<endl ;
    }
    else cout<<"article non disponible a l'umprunt "<<endl ;


}



void Article::ajoutGenre(const std::string& genre){

    if (_genres.size()<2) {
        _genres.push_back(genre);
    }
    else
    cout<<"ajout impossible"<<endl;

}




void Article::afficherGenre() const {

    if (_genres.size()!=0)
        
    {
        cout<<"genre(s) : "<<endl ;
        for (int i=0; i<_genres.size(); i++) {
            cout<<_genres[i]<<endl;
        }
    }
    else cout<<"aucun genre enregistré"<<endl ;


}




Disponibilite Article::getDisponibilite()const {
    return _disponibilite ;

}

void Article::setDisponibilite(const Disponibilite dispo){

    _disponibilite=dispo ;
}



std::string Article::getGenre(const int position) const {
    if (_genres.size()!=0 && position<_genres.size()) {
        
        return _genres[position];
    }
    string str="VIDE";
    return str ;
}













