//
//  Transaction.hpp
//  projet_mediatheque
//
//  Created by CB mac  on 16/11/2015.
//  Copyright © 2015 CB mac . All rights reserved.
//

#ifndef Transaction_hpp
#define Transaction_hpp

#include <stdio.h>
#include <iostream>
#include <map>
#include "Date.hpp"
#include "Article.hpp"
#include "Utilisateur.hpp"
#include "generatorId.hpp"


enum Etat
{
    AUCUNE , EN_COURS ,RETARD,RENDU, FINALISE
};
typedef enum Etat Etat;

class Transaction {
private:
    Etat _etat;
    Article * _articlePrete;
    std::string _id;
    Date _datePret;
    Date _dateRendu;
    Utilisateur* _emprunteur;
    
protected :
    static unsigned int _count;
    static std::map<std::string,Transaction*> _poolTransaction ;

public:
    
    Transaction(const Etat& etat = AUCUNE,const char& initiale='T');
    
    void setEtat(const Etat& etat);
    Etat getEtat() const;
     static unsigned int getTransactionCount();
    
    std::string getId() const;
    void setDatePret(const int jour, const int mois , const int annee);
    std::string getDatePret() const;
    void setDateRendu(const int jour, const int mois , const int annee);
    std::string getDateRendu() const;
    
    ~Transaction() ;
    
    //-----
    void pretLivre(const std::string& idBook,const std::string& idUser); // mod 17dec 23:23
    void pretCd(const std::string& idCd,const std::string& idUser);
    void pretDvd(const std::string& idDvd,const std::string& idUser);
    //-----
    
    Article* getArticleEmprunte() const ;
    Utilisateur* getEmprunteur() const ;
    
    
    
    static void addTransaction(Transaction*) ;
    static void deleteTransaction(Transaction*);
    static Transaction* getTransactionAdresse(const std::string&) ;
    static void deleteAllTransaction();
    
};
//------------
void menuTransaction() ;
Transaction* ajouterTransaction() ;
void ChoixmodifTransaction(int choix,Transaction* transac_a_modifier);
void ChoixmodifTransactionBDD(int choix,const std::string&);

//---------------

#endif /* Transaction_hpp */