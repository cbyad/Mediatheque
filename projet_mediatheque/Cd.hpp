//
//  Cd.hpp
//  projet_mediatheque
//
//  Created by CB mac  on 16/11/2015.
//  Copyright © 2015 CB mac . All rights reserved.
//

#ifndef Cd_hpp
#define Cd_hpp

#include <stdio.h>
#include <map>
#include "Article.hpp"
#include "generatorId.hpp"

class Cd: public Article {
    
private:
    
    std::string _Studio ;
    static std::map<std::string,Cd*> _poolCd;
    
    static unsigned int _countCd;
    

public:
    
    Cd(const std::string& titre="",const unsigned int duree=0,const std::string& studio="",const Disponibilite& dispo=DISPONIBLE,const char& initiale='C');
    
    void setStudio(const std::string& studio);
    virtual std::string getStudio()const ;
    virtual void afficher() const ;
    
    
    ~Cd() ;
    static unsigned int getCdCount() ;
    
    static void deleteAllCd();
    static Cd* getCdAdresse(const std::string& );
    static void addCd(Cd* cd); 
    static void deleteCd(Cd* cd);
    

};
//---------------------
void menuCd ();
Cd* ajouterCd();
void ChoixmodifCd(int choix, Cd* cd_a_modifier) ;
void  ChoixmodifCdBDD(int,const std::string& );

//--------------------



#endif /* Cd_hpp */
