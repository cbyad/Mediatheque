//
//  Personne.hpp
//  projet_mediatheque
//
//  Created by CB mac  on 10/11/2015.
//  Copyright © 2015 CB mac . All rights reserved.
//

#ifndef Personne_hpp
#define Personne_hpp

#include <stdio.h>
#include "Date.hpp"
#include "generatorId.hpp"
#include <iostream>


class Personne {
private:
    std::string _nom ;
    std::string _prenom ;
    std::string  _id;
    Date _date ;
    
protected:
     
    static unsigned int _count ;
    
public:
    Personne(const std::string& nom="",const std::string& prenom="",const char& initiale='P');
  
    virtual void afficher() const =0 ;
    
    void setNom(const std::string& nom);
    void setDate(const int jour , const int mois , const int annne);
    std::string getDate()const ;
    std::string getNom() const ;
    void setPrenom(const std::string& prenom);
    std::string getPrenom()const ;
    std::string getId() const;
    
   
    static unsigned int getPersonneCount() ;
    virtual ~Personne() ;
};






#endif /* Personne_hpp */
