//
//  Utilisateur.hpp
//  projet_mediatheque
//
//  Created by CB mac  on 10/11/2015.
//  Copyright © 2015 CB mac . All rights reserved.
//

#ifndef Utilisateur_hpp
#define Utilisateur_hpp

#include <stdio.h>
#include <iostream>
#include <list>
#include <map>
#include "Personne.hpp"
#include "Article.hpp"
#include "Livre.hpp"
#include "Cd.hpp"
#include "Dvd.hpp"

class Utilisateur: public Personne {
private:
    
    std::string _numeroTelephone;
    std::string _adresse ;
    std::list<Article*> _articleEmpruntes ;//// pour cet projet on limitera le nombre d'emprunt a 3 articles !
    
    static std::map<std::string,Utilisateur*> _poolUser ;
    static unsigned int _countUser ;
    
public:
    Utilisateur(const std::string& nom="",const std::string& prenom="",const std::string& adresse="",const std::string& numero="",const char& initiale='U');
    
    
    virtual void afficher() const ;//
    void setNumeroTelephone(const std::string& tel);
    std::string getNumeroTelephone()const ;
    void setAdresse(const std::string& adr);
    std::string getAdresse()const ;
  
    void setListeArticles(Article* article);//
     
    static unsigned int getCountUser() ;
    static void deleteAllUser(); // libère tous les users
    static Utilisateur* getUserAdresse( const std::string& );
    static void addUser(Utilisateur* user); // enregistre user dans le pool
    static void deleteUser(Utilisateur* user);
    
    ~Utilisateur() ;

    
};
void menuUtilisateur() ;
Utilisateur* ajouterUtilisateur();
void ChoixmodifUserBDD(int choix,const std::string& idUseramodifier);
void ChoixmodifUser(int choix,Utilisateur* user_a_modifier);



#endif /* Utilisateur_hpp */
