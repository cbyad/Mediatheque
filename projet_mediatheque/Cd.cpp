//
//  Cd.cpp
//  projet_mediatheque
//
//  Created by CB mac  on 16/11/2015.
//  Copyright © 2015 CB mac . All rights reserved.
//

#include "Cd.hpp"
using namespace std;
//------------------- Attribut de classe
unsigned int Cd::_countCd=0 ;
std::map<std::string,Cd*> Cd::_poolCd;

//---------------------


Cd::Cd(const std::string& titre,const unsigned int duree,const std::string& studio,const Disponibilite& dispo,const char& initiale):
Article(titre,duree,dispo,initiale),_Studio(studio)

{
    addCd(this);
    _countCd++;
}


void Cd::setStudio(const std::string& studio){ _Studio=studio ;}



string Cd::getStudio()const {return _Studio ;}


void Cd::afficher() const {
    cout << "Titre: " << getTitre() << endl;
    cout << "Date: " << getDate() << endl ;
    cout << "Duree: " << getDuree() << endl;
    cout << "Studio: " << getStudio() << endl;
    afficherGenre();
    cout << "Disponibilite: " << getDisponibilite() << endl;

}


Cd::~Cd() {
    deleteCd(this);
    _countCd--;
}


void Cd::deleteAllCd(){

    _poolCd.clear();

}


Cd* Cd::getCdAdresse(const std::string& id){

    map<string,Cd*>::iterator it=_poolCd.find(id);
    
    if (it == _poolCd.end()) {
        return NULL ;
    }
    return it->second ;

}


void Cd::addCd(Cd* cd)
{
    if(cd != NULL){
        _poolCd[cd->getId()]=cd;
    }

}


void Cd::deleteCd(Cd *cd){
 if(cd!=NULL) _poolCd.erase(cd->getId());

}



unsigned int Cd::getCdCount(){return _countCd;}

//-----------------------


void menuCd () {
    
    string continuer="OUI";
    
    int choix ;
    while (continuer=="OUI")
    {
        cin.clear();
        cin.ignore();
        
        cout << " 1. Ajouter un CD \n 2. Supprimer un CD \n 3. Modifier un CD \n 4. Obtenir une information  \n 5. Supprimer tous les CDs '/!\\' " << endl;
        cin >> choix;
        
        switch(choix)
        {
            case 1 :{
                cin.clear();
                cin.ignore();
                string sql ;
                Cd* cd ;
                cd=ajouterCd() ;
                
                if(cd != NULL){
                
                if(cd->getAuteur()!=NULL){
                 sql="INSERT INTO CD (ID,TITRE,STUDIO,DUREE,DATE,GENRE_1,DISPONIBILITE,AUTEUR,GENRE_2) " \
                "VALUES ('"+cd->getId()+"','"+cd->getTitre()+"','"+cd->getStudio()+"','"+int2String(cd->getDuree())+"','"+cd->getDate()+"',"\
                "'"+cd->getGenre(0)+"','"+int2String(cd->getDisponibilite())+"','"+cd->getAuteur()->getNom()+"' ,'"+cd->getGenre(1)+"') ";
               
                }
                else{
                     sql="INSERT INTO CD (ID,TITRE,STUDIO,DUREE,DATE,GENRE_1,DISPONIBILITE,AUTEUR,GENRE_2) " \
                    "VALUES ('"+cd->getId()+"','"+cd->getTitre()+"','"+cd->getStudio()+"','"+int2String(cd->getDuree())+"','"+cd->getDate()+"',"\
                    "'"+cd->getGenre(0)+"','"+int2String(cd->getDisponibilite())+"','INCONNU' ,'"+cd->getGenre(1)+"') ";
                    
                }
                
                const char* conversionSql = sql.c_str() ;
                
               
                    OperationBDD(conversionSql); //ajout de l'info dans mediatheque.db
                }
                
        
                else cout << "Ajout de l'information impossible dans la base de donnee" << endl ;
                break;
                
            }
                
            case 2 : {
                cin.clear();
                cin.ignore();
                string idCDaSupprimer ;
                OperationBDD("SELECT ID,TITRE,AUTEUR from CD");
                cout << "Saisissez l'ID du CD a supprimer: \t" ;
                getline(cin,idCDaSupprimer);
                Cd* cd = Cd::getCdAdresse(idCDaSupprimer);
                
                string sql= "DELETE from CD WHERE ID='"+idCDaSupprimer+"'" ;
                
                const char* conversionSql = sql.c_str() ;
                if (cd==NULL) {
                    
                    OperationBDD(conversionSql);     //supprimer bdd
                    
                }
                else {
                    Cd::deleteCd(cd) ;//supprimer du pool
                    OperationBDD(conversionSql);    //supprimer bdd
                    
                }
                
                break ;
            }
            case 3:{
                cin.clear();
                cin.ignore();
                string idCDamodifier ;
                OperationBDD("SELECT ID,TITRE,AUTEUR from CD");
                cout<<"Saisissez l'id du Cd a modifier \t";
                getline(cin,idCDamodifier);
                Cd* cd = Cd::getCdAdresse(idCDamodifier);
                if (cd == NULL) {
                    cout << "Vous souhaitez modifier ..." << endl;
                    cout << "1. Le titre\n2. La date de sortie\n3. L'artiste\n4. La duree\n5. Le studio\n6. Le genre" << endl;
                    cin >> choix;
                    ChoixmodifCdBDD(choix,idCDamodifier);
                    
                    
                }
                else{
                    cout << "Vous souhaitez modifier ..." << endl;
                    cout << "1. Le titre\n2. La date de sortie\n3. L'artiste\n4. La duree\n5. Le studio\n6. Le genre" << endl;
                    cin >> choix;
                    ChoixmodifCd(choix,cd);
                }
                
                break ;
            }
            case 4:{
                cin.clear();
                cin.ignore();
                string idCDaAfficher ;
                OperationBDD("SELECT ID,TITRE,AUTEUR from CD");
                cout << "Saisissez ID du CD a afficher:  \t" ;
                getline(cin,idCDaAfficher);
                
                string sql = "SELECT * from CD where ID='"+idCDaAfficher+"';" ;//
                
                const char* conversionSql = sql.c_str() ;
                OperationBDD(conversionSql);
                break ;
            }
            case 5:{
                Cd::deleteAllCd() ;
                OperationBDD("DELETE  FROM  CD ;");
                break ;
            }
                
            default:
                
                break ;
        }
        cout << "Souhaitez vous avoir une autre action sur un CD ? oui/non" << endl;
        cin >> continuer;
        majuscule(continuer);
    }
    
}


Cd* ajouterCd() {

    //-------------
    cin.clear();
   // cin.ignore();
    //--------------
    
    int jour , mois ,annee ;
    string genre; string reponse ;
    string idAuteur ; string titre ; string studio ;
    
    int duree ;
    cout<<"saisir le titre"<<endl;
    
    getline(cin,titre);
    
    cout<<"saisir le nom du studio"<<endl;
    
    getline(cin,studio);
    cout<<"saisir la duree"<<endl;
    
 
    cin>>duree ;
   
    Cd* cd=new Cd(titre,duree,studio);
    
    cout<<"saisir la date de creation du cd"<<endl ;
    cout<<"le jour : \t";
    cin>>jour ;
    cout<<"le mois : \t";
    cin>>mois ;
    cout<<"l'annee : \t";
    cin>>annee ;
    
    cd->setDate(jour, mois, annee);
    cin.clear();
    cin.ignore();
    cout<<"saisir l'identifiant de l'auteur du Cd"<<endl;

    getline(cin, idAuteur);
    
    cd->associer(idAuteur);////
    
    cout<<"ajoutez un genre particulier \t" ;
   
    getline(cin,genre);
    cd->ajoutGenre(genre);
    
    cout<<"souhaitez vous ajoutez un autre genre particulier ? oui/non \t ";
    
    getline(cin,reponse);
    majuscule(reponse);
    
    if (reponse=="OUI") {
        cout<<"saisir le genre \t ";
        getline(cin,genre);
        cd->ajoutGenre(genre);
    }
    
    return cd ;
    
}



void ChoixmodifCd(int choix, Cd* cd_a_modifier) //Implémenter les modif dans la base de donnée.
{
    switch(choix){
           
        case 1 :
        {
            cin.clear();
            cin.ignore();
            string titre;
            cout << "Saisir le titre" << endl;
            getline(cin,titre);
            cd_a_modifier->setTitre(titre);
            
            string sql="UPDATE CD SET TITRE='"+titre+"' WHERE ID='"+cd_a_modifier->getId()+"' ";
             const char* conversionSql=sql.c_str() ;
            OperationBDD(conversionSql);
    
            break;
        }
        case 2 :
        {
            cin.clear();
            cin.ignore();
            int jour,mois,annee;
            cout << "Saisir la nouvelle date de creation du cd :" << endl ;
            cout << "Le jour : \t " ;cin >> jour ;
            cout << "Le mois : \t"; cin >> mois ;
            cout << "L'annee : \t";cin >> annee ;
            cd_a_modifier->setDate(jour, mois, annee);
            string date=cd_a_modifier->getDate();
            
            string sql="UPDATE CD SET DATE='"+date+"' WHERE ID='"+cd_a_modifier->getId()+"'";
            const char* conversionSql=sql.c_str() ;
            OperationBDD(conversionSql);
        
            break;
        }
        case 3 :
        {
            cin.clear();
            cin.ignore();
            string IDartiste;
            cout << "Entrez l'id de l'artiste" << endl;
            getline(cin,IDartiste);
            cd_a_modifier->associer(IDartiste);
            string nomArtiste=cd_a_modifier->getAuteur()->getNom() ;
            
            string sql="UPDATE CD SET AUTEUR='"+nomArtiste+"' WHERE ID='"+cd_a_modifier->getId()+"' ";
            const char* conversionSql=sql.c_str() ;
            OperationBDD(conversionSql);
           
            break;
        }
        case 4 :
        {
            cin.clear();
            cin.ignore();
            int duree;
            cout << "Entrez la nouvelle durée" << endl;
            cin >> duree;
            cd_a_modifier->setDuree(duree);
            
            string sql="UPDATE CD SET DUREE="+int2String(cd_a_modifier->getDuree())+" WHERE ID='"+cd_a_modifier->getId()+"' ";
            const char* conversionSql=sql.c_str() ;
            OperationBDD(conversionSql);
            
            
            break;
        }
        case 5 :
        {
            cin.clear();
            cin.ignore();
            string studio;
            cout << "Entrez le nouveau studio" << endl;
            getline(cin,studio);
            cd_a_modifier->setStudio(studio);
            
            string sql="UPDATE CD SET STUDIO='"+studio+"' WHERE ID='"+cd_a_modifier->getId()+"' ";
            const char* conversionSql=sql.c_str() ;
            OperationBDD(conversionSql);

            
            break;
        }
        case 6 :
        {
            cin.clear();
            cin.ignore();
            string genre1, genre2;
            cout << "Merci de redefinir le genre, avec un maximum de deux attributs" << endl;
            getline(cin,genre1);
            getline(cin,genre2);
            cd_a_modifier->setGenre(genre1, genre2);
            
            string sql="UPDATE CD SET GENRE_1='"+genre1+"' WHERE ID='"+cd_a_modifier->getId()+"' ";
            const char* conversionSql=sql.c_str() ;
            OperationBDD(conversionSql);
            
            string sql2="UPDATE CD SET GENRE_2='"+genre2+"' WHERE ID='"+cd_a_modifier->getId()+"' ";
            const char* conversionSql2=sql2.c_str() ;
            OperationBDD(conversionSql2);
            
            
            break;
        }
            
    }
}


void ChoixmodifCdBDD(int choix,const string& idCDamodifier ){


    switch(choix){
        case 1 :
        {
            cin.clear();
            cin.ignore();
            string titre;
            cout << "Saisir le titre " << endl;
            getline(cin,titre);
            
            string sql="UPDATE CD SET TITRE='"+titre+"' WHERE ID='"+idCDamodifier+"' ";
            const char* conversionSql=sql.c_str() ;
            OperationBDD(conversionSql);
            
            break;
        }
        case 2 :
        {
            cin.clear();
            cin.ignore();
            int jour,mois,annee;
            cout << "Saisir la nouvelle date de creation du cd :" << endl ;
            cout << "Le jour : \t " ;cin >> jour ;
            cout << "Le mois : \t"; cin >> mois ;
            cout << "L'annee : \t";cin >> annee ;
          
            string date=int2String(jour)+"/"+int2String(mois)+"/"+int2String(annee);
          
            
            string sql="UPDATE CD SET DATE='"+date+"' WHERE ID='"+idCDamodifier+"'";
            const char* conversionSql=sql.c_str() ;
            OperationBDD(conversionSql);
            
            break;
        }
        case 3 :
        {
            cin.clear();
            cin.ignore();
            string nomArtiste;
            cout << "Entrez l'id de l'artiste" << endl;
            getline(cin,nomArtiste);
       
          
            string sql="UPDATE CD SET AUTEUR='"+nomArtiste+"' WHERE ID='"+idCDamodifier+"' ";
            const char* conversionSql=sql.c_str() ;
            OperationBDD(conversionSql);
            
            break;
        }
        case 4 :
        {
            cin.clear();
            cin.ignore();
            int duree;
            cout << "Entrez la nouvelle durée" << endl;
            cin >> duree;
        
            string sql="UPDATE CD SET DUREE="+int2String(duree)+" WHERE ID='"+idCDamodifier+"' ";
            const char* conversionSql=sql.c_str() ;
            OperationBDD(conversionSql);
            
            
            break;
        }
        case 5 :
        {
            cin.clear();
            cin.ignore();
            string studio;
            cout << "Entrez le nouveau studio" << endl;
            getline(cin,studio);
            
            
            string sql="UPDATE CD SET STUDIO='"+studio+"' WHERE ID='"+idCDamodifier+"' ";
            const char* conversionSql=sql.c_str() ;
            OperationBDD(conversionSql);
            
            
            break;
        }
        case 6 :
        {
            cin.clear();
            cin.ignore();
            string genre1, genre2;
            cout << "Merci de redefinir le genre, avec un maximum de deux attributs" << endl;
            getline(cin,genre1);
            getline(cin,genre2);
            
            
            string sql="UPDATE CD SET GENRE_1='"+genre1+"' WHERE ID='"+idCDamodifier+"' ";
            const char* conversionSql=sql.c_str() ;
            OperationBDD(conversionSql);
            
            string sql2="UPDATE CD SET GENRE_2='"+genre2+"' WHERE ID='"+idCDamodifier+"' ";
            const char* conversionSql2=sql2.c_str() ;
            OperationBDD(conversionSql2);
            
            
            break;
        }
            
    }


}

























