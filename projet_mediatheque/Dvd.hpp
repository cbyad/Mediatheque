//
//  Dvd.hpp
//  projet_mediatheque
//
//  Created by CB mac  on 16/11/2015.
//  Copyright © 2015 CB mac . All rights reserved.
//

#ifndef Dvd_hpp
#define Dvd_hpp

#include <stdio.h>
#include <vector>
#include <map>
#include "Article.hpp"


class Dvd :public Article{
    
    std::vector<std::string> _castings ;
    std::string _Studio ;
    static unsigned int _countDvd;
    static std::map<std::string,Dvd*> _poolDvd ;
    
    
    
public:
    
    Dvd(const std::string& titre="", const unsigned int duree=0 ,const std::string& studio="",const Disponibilite& dispo=DISPONIBLE,const char& initiale='D');
    
    void setStudio(const std::string& );
    virtual std::string getStudio()const ;
    
    void setCasting(const std::string&);
    void printCasting()  const ;
    std::string getCasting(const int position) const ;
    virtual void afficher() const ;
    
    static unsigned int getCountDvd();
    
    static void deleteAllDvd();
    static Dvd* getDvdAdresse(const std::string& );
    static void addDvd(Dvd* );
    static void deleteDvd(Dvd* );
    
    ~Dvd() ;
};


//----------
void menuDvd() ;
Dvd* ajouterDvd() ;
void ChoixmodifDvdBDD(int choix,const std::string& idDVDamodifier );
void ChoixmodifDvd(int choix, Dvd* dvd_a_modifier);

//-----------



#endif /* Dvd_hpp */
